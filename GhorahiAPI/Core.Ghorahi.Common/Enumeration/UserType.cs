﻿namespace Core.Ghorahi.Common.Enumeration
{
    public enum UserType
    {
        Agent = 1,
        Customer = 2
    }
}