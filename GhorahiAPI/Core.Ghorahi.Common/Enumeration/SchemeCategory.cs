﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Common.Enumeration
{
    public enum SchemeCategory
    {
        NoCategory = 0,
        Makhalu = 1,
        Lhotse = 2,
        Kanchanjunga = 3,
        K2 = 4,
        Sagarmatha = 5
    }
}
