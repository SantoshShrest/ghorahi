﻿namespace Core.Ghorahi.Common.Enumeration
{
    public enum OrderStatus
    {
        Open = -101,
        Cancel = -102,
        Release = -103,
        ShortClose = -104,
        Close = -105,
        Completed = -106,
        Hold = -107
    }

    public enum OrderStates
    {
        Sales = 1,
        Dispatch = 2,
        Invoice = 3
    }
}