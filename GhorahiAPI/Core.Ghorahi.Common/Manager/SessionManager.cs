﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Common.Manager
{
    public sealed class SessionManager
    {
        public static string UserName { get; set; }
        public static bool IsCustomer { get; set; }
    }
}