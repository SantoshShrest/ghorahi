﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Common.Manager
{
    public class DataManager
    {
        public static List<NotificationResponse> CustomerSalesNotifications { get; set; }
        public static List<NotificationResponse> CustomerDispatchNotifications { get; set; }
        public static string SalesOrderNotificationDate { get; set; }
        public static string SalesDispatchNOficationDate { get; set; }
    }

    public class NotificationResponse
    {
        public long CustomerId { get; set; }
        public long AgentId { get; set; }
        public long OrderId { get; set; }
        public string ItemName { get; set; }
        public decimal? ItemQuantity { get; set; }
        public string OrderDate { get; set; }
        public long OrderStatus { get; set; }
        public bool IsNotified { get; set; }
        public string LastModifiedDate { get; set; }
        public int ResponseType { get; set; }
    }
}