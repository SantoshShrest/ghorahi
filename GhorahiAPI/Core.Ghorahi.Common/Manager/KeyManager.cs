﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Common.Manager
{
    public sealed class KeyManager
    {
        public static string GenerateNewApiKey(string userName)
        {
            string salt = $"{userName}g#0r!H!@pI{DateTime.Now.ToString("ddMMyyyy")}";
            string chiperText = "";
            using (SHA512 sha = SHA512.Create())
            {
                chiperText = BitConverter.ToString(sha.ComputeHash(Encoding.UTF8.GetBytes(salt))).Replace("-", "").ToLower();
            }
            return chiperText;
        }
    }
}