﻿using Core.Ghorahi.Data.Entity;
using Core.Ghorahi.Data.Entity.Inventory;
using Core.Ghorahi.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Data
{
    public class Repository : IRepository
    {
        public TCDBizBookEntities context;
        public InventoryEntities inventoryContext;
        public Repository()
        {
            context = new TCDBizBookEntities();
            inventoryContext = new InventoryEntities();
        }

        public TCDBizBookEntities Context { get { return context; } }
        public InventoryEntities InventoryContext { get { return inventoryContext; } }

        public IQueryable<TEntity> All<TEntity>() where TEntity : class
        {
            throw new NotImplementedException();
        }

        public bool Any<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public IQueryable<TEntity> Filter<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public TEntity Find<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public TEntity GetById<TEntity>(object id) where TEntity : class
        {
            throw new NotImplementedException();
        }

        public void Insert<TEntity>(TEntity entity) where TEntity : class
        {
            context.Set<TEntity>().Add(entity);
        }

        public int SaveChanges()
        {
            return context.SaveChanges();
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class
        {
            context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }
    }
}