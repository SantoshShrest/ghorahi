﻿using Core.Ghorahi.Data.Entity;
using Core.Ghorahi.Data.Entity.Inventory;
using System;
using System.Linq;
using System.Linq.Expressions;

namespace Core.Ghorahi.Data.Interface
{
    public interface IRepository
    {
        TCDBizBookEntities Context { get; }
        InventoryEntities InventoryContext { get; }
        IQueryable<TEntity> All<TEntity>() where TEntity : class;
        IQueryable<TEntity> Filter<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;
        TEntity Find<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;
        TEntity GetById<TEntity>(object id) where TEntity : class;
        bool Any<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class;
        void Insert<TEntity>(TEntity entity) where TEntity : class;
        void Update<TEntity>(TEntity entity) where TEntity : class;
        int SaveChanges();
    }
}