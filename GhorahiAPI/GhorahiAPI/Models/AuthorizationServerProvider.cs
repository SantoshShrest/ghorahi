﻿using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Service.Models.Messages;
using Core.Ghorahi.Service.Models.Users;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;

namespace GhorahiAPI.Models
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        private IAuthenticateService authenticateService;
        public AuthorizationServerProvider(IAuthenticateService authenticateService)
        {
            this.authenticateService = authenticateService;
        }
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            AuthenticateUser authenticateUser = new AuthenticateUser { UserName = context.UserName, Password = context.Password };
            ResponseMessage<Core.Ghorahi.Service.Models.Users.Result.AuthenticateResult> responseMessage = authenticateService.VerifyUser(authenticateUser);
            if (!responseMessage.HasError && responseMessage.ResponseValue.IsVerified)
            {
                identity.AddClaim(new Claim("Id", responseMessage.ResponseValue.Id.ToString()));
                var props = new AuthenticationProperties(new Dictionary<string, string> { { "user", context.UserName }, { "role", responseMessage.ResponseValue.UserType.ToString() } });
                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is incorrect");
                context.Rejected();
            }
            return;
        }
    }
}