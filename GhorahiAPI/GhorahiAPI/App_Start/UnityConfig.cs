using Core.Ghorahi.Data;
using Core.Ghorahi.Data.Interface;
using Core.Ghorahi.Service.Implementation.Agents;
using Core.Ghorahi.Service.Implementation.Authenticates;
using Core.Ghorahi.Service.Implementation.Customers;
using Core.Ghorahi.Service.Implementation.Sales;
using Core.Ghorahi.Service.Implementation.Stocks;
using Core.Ghorahi.Service.Interfaces;
using System;

using Unity;

namespace GhorahiAPI
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IRepository, Repository>();
            container.RegisterType<ISaleService, SaleService>();
            container.RegisterType<IAuthenticateService, AuthenticateService>();
            container.RegisterType<IStockService, StockService>();
            container.RegisterType<IAgentService, AgentService>();
            container.RegisterType<ICustomerService, CustomerService>();
        }
    }
}