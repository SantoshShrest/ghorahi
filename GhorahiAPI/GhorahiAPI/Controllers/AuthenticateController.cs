﻿using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Service.Models.Users;
using Core.Ghorahi.Service.Models.Users.Result;
using Newtonsoft.Json.Linq;
using System.Web.Http;
using System.Web.Security;

namespace GhorahiAPI.Controllers
{
    [RoutePrefix("api/authenticate")]
    public class AuthenticateController : ApiController
    {
        private IAuthenticateService authenticateService;
        public AuthenticateController(IAuthenticateService authenticateService)
        {
            this.authenticateService = authenticateService;
        }

        [Route("verifyuser")]
        [HttpPost]
        public IHttpActionResult VerifyUser([FromBody]JObject userData)
        {
            AuthenticateUser authenticateUser = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthenticateUser>(userData.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<AuthenticateResult> responseMessage = authenticateService.VerifyUser(authenticateUser);
            if (responseMessage.ResponseValue.IsVerified)
            {
                FormsAuthentication.SetAuthCookie(authenticateUser.UserName, true);
            }
            return Ok(responseMessage);
        }
    }
}