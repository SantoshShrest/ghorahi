﻿using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Service.Models;
using System.Collections.Generic;
using GhorahiAPI.Attribute;
using System.Web.Http;
using Core.Ghorahi.Service.Models.Stocks.Request;
using Core.Ghorahi.Service.Models.Stocks;

namespace GhorahiAPI.Controllers
{
    [RoutePrefix("api/stocks")]
    [UserAuthorize]
    public class StockController : ApiController
    {
        private IStockService stockService;
        public StockController(IStockService stockService)
        {
            this.stockService = stockService;
        }

        [Route("items")]
        [HttpGet]
        public IHttpActionResult StockItems()
        {
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SelectItem>> responseMessage = stockService.GetStockItems();
            return Ok(responseMessage);
        }

        [Route("pricerate")]
        [HttpPost]
        public IHttpActionResult GetStockItemPriceRate(ItemPriceRateRequest itemPriceRateRequest)
        {
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<ItemPriceRate> responseMessage = stockService.GetStockItemPriceRate(itemPriceRateRequest);
            return Ok(responseMessage);
        }
    }
}