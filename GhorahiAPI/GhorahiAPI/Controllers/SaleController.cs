﻿using Core.Ghorahi.Service.Models.Sales.SpResult;
using Core.Ghorahi.Service.Models.Sales;
using Core.Ghorahi.Service.Interfaces;
using System.Collections.Generic;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using GhorahiAPI.Attribute;
using Core.Ghorahi.Service.Models;
using Core.Ghorahi.Service.Models.Sales.Request;

namespace GhorahiAPI.Controllers
{
    [RoutePrefix("api/sales")]
    [ApiAuthorize]
    public class SaleController : ApiController
    {
        public ISaleService saleService;
        public SaleController(ISaleService saleService)
        {
            this.saleService = saleService;
        }

        [Route("order")]
        [HttpPost]
        public IHttpActionResult CreateOrder([FromBody]JObject request)
        {
            SalesOrderRequest salesOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<SalesOrderRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<int> responseMessage = saleService.SaveSalesOrder(salesOrder);
            return Ok(responseMessage);
        }

        [Route("order/register")]
        [HttpPost]
        public IHttpActionResult Order([FromBody]JObject request)
        {
            SalesOrderRegisterRequest salesOrderRegister = Newtonsoft.Json.JsonConvert.DeserializeObject<SalesOrderRegisterRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SalesOrderRegister>> responseMessage = saleService.GetSalesOrdersRegister(salesOrderRegister);
            return Ok(responseMessage);
        }

        [Route("dispatch/register")]
        [HttpPost]
        public IHttpActionResult Dispatch([FromBody]JObject request)
        {
            SalesDispatchRegisterRequest salesDispatchRegister = Newtonsoft.Json.JsonConvert.DeserializeObject<SalesDispatchRegisterRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SalesDispatchRegister>> responseMessage = saleService.GetSalesDispatchRegister(salesDispatchRegister);
            return Ok(responseMessage);
        }

        [Route("invoice/register")]
        [HttpPost]
        public IHttpActionResult Invoice([FromBody]JObject request)
        {
            SalesInvoiceRegisterRequest salesInvoiceRegister = Newtonsoft.Json.JsonConvert.DeserializeObject<SalesInvoiceRegisterRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SalesInvoiceRegister>> responseMessage = saleService.GetSalesInvoiceRegister(salesInvoiceRegister);
            return Ok(responseMessage);
        }

        [Route("ledger/register")]
        [HttpPost]
        public IHttpActionResult Ledger([FromBody]JObject request)
        {
            SalesLedgerRequest salesLedger = Newtonsoft.Json.JsonConvert.DeserializeObject<SalesLedgerRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SalesLedgerRegister>> responseMessage = saleService.GetSalesLedgerRegister(salesLedger);
            return Ok(responseMessage);
        }

        [Route("order/hold")]
        [HttpPost]
        public IHttpActionResult GetHoldSalesOrder([FromBody]JObject request)
        {
            HoldSalesOrder holdSalesOrder = Newtonsoft.Json.JsonConvert.DeserializeObject<HoldSalesOrder>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SalesOrderHold>> responseMessage = saleService.GetHoldSalesOrder(holdSalesOrder);
            return Ok(responseMessage);
        }

        [Route("order/update/status")]
        [HttpPost]
        public IHttpActionResult UpdateSalesOrderStatus([FromBody]JObject request)
        {
            UpdateOrderStatusRequest updateOrderStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<UpdateOrderStatusRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<int> responseMessage = saleService.UpdateOrderStatus(updateOrderStatus);
            return Ok(responseMessage);
        }
    }
}