﻿using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Service.Models.Customers;
using Core.Ghorahi.Service.Models.Customers.Request;
using Core.Ghorahi.Service.Models.Customers.Response;
using GhorahiAPI.Attribute;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GhorahiAPI.Controllers
{
    [RoutePrefix("api/customers")]
    [ApiAuthorize]
    public class CustomerController : ApiController
    {
        private ICustomerService customerService;
        public CustomerController(ICustomerService customerService)
        {
            this.customerService = customerService;
        }

        [Route("sahabhagi/scheme")]
        [HttpPost]
        public IHttpActionResult SahabhagiDetails([FromBody]JObject request)
        {
            SahabhagiSchemeRequest sahyogiSchemeRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<SahabhagiSchemeRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<SahyogiSchemeResponse> responseMessage = customerService.GetSahyogiScheme(sahyogiSchemeRequest.CustomerId);
            return Ok(responseMessage);
        }

        [Route("sahabhagi/details")]
        [HttpPost]
        public IHttpActionResult SahabhagiSchemeDetails([FromBody]JObject request)
        {
            SahabhagiSchemeRequest sahyogiSchemeRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<SahabhagiSchemeRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SahabhagiSchemeDetailDTOs>> responseMessage = customerService.GetSahabhagiSchemeDetails(sahyogiSchemeRequest.CustomerId);
            return Ok(responseMessage);
        }

        [Route("sahabhagi/facilities")]
        [HttpPost]
        public IHttpActionResult SahabhagiSchemeFacilities([FromBody]JObject request)
        {
            SahabhagiSchemeRequest sahyogiSchemeRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<SahabhagiSchemeRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SahabhagiSchemeFacilityDTOs>> responseMessage = customerService.GetSahabhagiSchemeFacilities(sahyogiSchemeRequest.CustomerId);
            return Ok(responseMessage);
        }

        [Route("sahabhagi/discounted/invoice")]
        [HttpPost]
        public IHttpActionResult SahabhagiDiscountedInvoice([FromBody]JObject request)
        {
            SahabhagiSchemeRequest sahyogiSchemeRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<SahabhagiSchemeRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SalesInvoiceResult>> responseMessage = customerService.GetDiscountedBags(sahyogiSchemeRequest.CustomerId);
            return Ok(responseMessage);
        }

        [Route("register/order/notice")]
        [HttpPost]
        [CheckNotification]
        public IHttpActionResult GetRegisterOrderNotice([FromBody] JObject request)
        {
            CustomerNotificationRequest notificationRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<CustomerNotificationRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<CustomerNotificationResponse>> responseMessage = customerService.GetRegisterOrderNotice(notificationRequest.CustomerId);
            return Ok(responseMessage);
        }

        [Route("dispatch/order/notice")]
        [HttpPost]
        [CheckNotification]
        public IHttpActionResult GetDispatchOrderNotice([FromBody]JObject request)
        {
            CustomerNotificationRequest notificationRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<CustomerNotificationRequest>(request.ToString());
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<CustomerNotificationResponse>> responseMessage = customerService.GetDispatchOrderNotice(notificationRequest.CustomerId);
            return Ok(responseMessage);
        }
    }
}