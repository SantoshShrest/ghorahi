﻿using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Service.Models;
using GhorahiAPI.Attribute;
using System.Collections.Generic;
using System.Web.Http;

namespace GhorahiAPI.Controllers
{
    [RoutePrefix("api/agents")]
    public class AgentController : ApiController
    {
        private IAgentService agentService;
        public AgentController(IAgentService agentService)
        {
            this.agentService = agentService;
        }
        
        [Route("{agentId}/customer")]
        [AgentAuthorize]
        public IHttpActionResult GetCustomers(long agentId)
        {
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SelectItem>> responseMessage = agentService.GetCustomersByAgent(agentId);
            return Ok(responseMessage);
        }

        [Route]
        [AdminAuthorize]
        public IHttpActionResult GetAgents()
        {
            Core.Ghorahi.Service.Models.Messages.ResponseMessage<List<SelectItem>> responseMessage = agentService.GetAgents();
            return Ok(responseMessage);
        }
    }
}