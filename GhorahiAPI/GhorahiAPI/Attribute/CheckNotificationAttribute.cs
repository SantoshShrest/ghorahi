﻿using Core.Ghorahi.Common.Enumeration;
using Core.Ghorahi.Common.Manager;
using Core.Ghorahi.Data;
using Core.Ghorahi.Data.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using UnconstrainedMelody;

namespace GhorahiAPI.Attribute
{
    public class CheckNotificationAttribute : FilterAttribute, IActionFilter
    {
        public IRepository Repository { get { return new Repository(); } }
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {

        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                string[] routeNames = filterContext.HttpContext.Request.Path.Split('/');
                long[] orderStatus = Enums.GetValues<OrderStatus>().Select(s => (long)s).ToArray();
                switch (routeNames[1])
                {
                    case "register":
                        SalesOrderRegisterNotification(orderStatus);
                        break;
                    case "dispatch":
                        SalesDispatchRegisterNofication(orderStatus);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SalesOrderRegisterNotification(long[] orderStatus)
        {
            if (!DataManager.SalesOrderNotificationDate.Equals(DateTime.Now.ToString("yyyymmdd")))
            {
                DataManager.SalesOrderNotificationDate = DateTime.Now.ToString("yyyymmdd");
                DataManager.CustomerSalesNotifications = Repository.Context.SalesOrderDetails.Where(w => orderStatus.Contains(w.OrderStatusID) && w.SalesOrder.LastModifDatestamp.Equals(DataManager.SalesOrderNotificationDate))
                    .Select(s => new NotificationResponse
                    {
                        AgentId = s.SalesOrder.ShipmentAgentID,
                        CustomerId = s.SalesOrder.CustomerID,
                        IsNotified = false,
                        ItemName = s.StockItem.Name,
                        ItemQuantity = s.Qty,
                        OrderDate = s.SalesOrder.OrderDate,
                        OrderStatus = s.OrderStatusID,
                        OrderId = s.SalesOrderID,
                        LastModifiedDate = s.SalesOrder.LastModifDatestamp,
                        ResponseType = (int)OrderStates.Sales
                    }).ToList();
            }
            else
            {
                long[] salesOrderIds = DataManager.CustomerSalesNotifications.Select(s => s.OrderId).ToArray();
                DataManager.CustomerSalesNotifications.AddRange(Repository.Context.SalesOrderDetails.Where(w => orderStatus.Contains(w.OrderStatusID) && !salesOrderIds.Contains(w.SalesOrder.ID) && w.SalesOrder.LastModifDatestamp.Equals(DataManager.SalesOrderNotificationDate))
                    .Select(s => new NotificationResponse
                    {
                        AgentId = s.SalesOrder.ShipmentAgentID,
                        CustomerId = s.SalesOrder.CustomerID,
                        IsNotified = false,
                        ItemName = s.StockItem.Name,
                        ItemQuantity = s.Qty,
                        OrderDate = s.SalesOrder.OrderDate,
                        OrderStatus = s.OrderStatusID,
                        OrderId = s.SalesOrderID,
                        LastModifiedDate = s.SalesOrder.LastModifDatestamp,
                        ResponseType = (int)OrderStates.Sales
                    }).ToList());
            }
        }

        private void SalesDispatchRegisterNofication(long[] orderStatus)
        {
            if (!DataManager.SalesDispatchNOficationDate.Equals(DateTime.Now.ToString("yyyymmdd")))
            {
                DataManager.SalesDispatchNOficationDate = DateTime.Now.ToString("yyyymmdd");
                DataManager.CustomerDispatchNotifications = Repository.Context.SalesDispatchDetails.Where(w => orderStatus.Contains(w.OrderStatusID) && w.SalesDispatch.LastModifDatestamp.Equals(DataManager.SalesOrderNotificationDate))
                    .Select(s => new NotificationResponse
                    {
                        AgentId = s.SalesDispatch.ShipmentAgentID,
                        CustomerId = s.SalesDispatch.CustomerID,
                        IsNotified = false,
                        ItemName = s.StockItem.Name,
                        ItemQuantity = s.Qty,
                        OrderDate = s.SalesOrder.OrderDate,
                        OrderStatus = s.OrderStatusID,
                        OrderId = s.SalesOrderID,
                        LastModifiedDate = s.SalesOrder.LastModifDatestamp,
                        ResponseType = (int)OrderStates.Sales
                    }).ToList();
            }
            else
            {
                long[] salesOrderIds = DataManager.CustomerSalesNotifications.Select(s => s.OrderId).ToArray();
                DataManager.CustomerDispatchNotifications.AddRange(Repository.Context.SalesDispatchDetails.Where(w => orderStatus.Contains(w.OrderStatusID) && !salesOrderIds.Contains(w.SalesOrder.ID) && w.SalesDispatch.LastModifDatestamp.Equals(DataManager.SalesOrderNotificationDate))
                    .Select(s => new NotificationResponse
                    {
                        AgentId = s.SalesDispatch.ShipmentAgentID,
                        CustomerId = s.SalesDispatch.CustomerID,
                        IsNotified = false,
                        ItemName = s.StockItem.Name,
                        ItemQuantity = s.Qty,
                        OrderDate = s.SalesOrder.OrderDate,
                        OrderStatus = s.OrderStatusID,
                        OrderId = s.SalesOrderID,
                        LastModifiedDate = s.SalesOrder.LastModifDatestamp,
                        ResponseType = (int)OrderStates.Sales
                    }).ToList());
            }
        }
    }
}