﻿using Core.Ghorahi.Service.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace GhorahiAPI.Attribute
{
    public class AgentAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (Authorize(filterContext))
            {
                return;
            }
            HandleUnauthorizedRequest(filterContext);
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }

        private bool Authorize(HttpActionContext actionContext)
        {
            bool isAuthorize = false;
            try
            {
                string[] authorizeParameter = actionContext.Request.Headers.Authorization.Parameter.Split('|');
                long userType = long.Parse(authorizeParameter[0]);
                string token = authorizeParameter[2];
                if (userType == -102)
                {
                    long userId = long.Parse(authorizeParameter[1]);
                    if (userId > 0 && !string.IsNullOrEmpty(token))
                    {
                        userId = Core.Ghorahi.Service.Implementation.Authenticates.Scheduler.Authenticate.GetInventoryAgentId(userId, userType);
                        ResponseMessage<bool> responseMessage = Core.Ghorahi.Service.Implementation.Authenticates.Scheduler.Authenticate.VerifyToken(userId, token);
                        if (!responseMessage.HasError)
                        {
                            isAuthorize = responseMessage.ResponseValue;
                        }
                    }
                }
                else if (userType == 786)
                {
                    string adminId = authorizeParameter[1];
                    if (!string.IsNullOrWhiteSpace(adminId))
                    {
                        ResponseMessage<bool> responseMessage = Core.Ghorahi.Service.Implementation.Authenticates.Scheduler.Authenticate.VerifyToken(adminId, token);
                        if (!responseMessage.HasError)
                        {
                            isAuthorize = responseMessage.ResponseValue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isAuthorize = false;
            }
            return isAuthorize;
        }
    }
}