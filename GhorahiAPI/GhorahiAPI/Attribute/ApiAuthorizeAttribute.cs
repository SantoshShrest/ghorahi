﻿using Core.Ghorahi.Common.Manager;
using Core.Ghorahi.Service.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace GhorahiAPI.Attribute
{
    public class ApiAuthorizeAttribute : AuthorizeAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (Authorize(filterContext))
            {
                return;
            }
            HandleUnauthorizedRequest(filterContext);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <returns></returns>
        private bool Authorize(HttpActionContext actionContext)
        {
            bool isAuthorize = false;
            try
            {
                string[] authorizeParameter = actionContext.Request.Headers.Authorization.Parameter.Split('|');
                Dictionary<string, object> jData = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, object>>(actionContext.Request.Content.ReadAsStringAsync().Result);
                long userType = long.Parse(authorizeParameter[0]);
                string token = authorizeParameter[1];
                long userId = 0;
                string adminId = "";                
                if (userType == 786)
                {
                    SessionManager.IsCustomer = false;
                    adminId = (string)jData.FirstOrDefault(fd => fd.Key.Equals("AdminId")).Value;
                    ResponseMessage<bool> responseMessage = Core.Ghorahi.Service.Implementation.Authenticates.Scheduler.Authenticate.VerifyToken(adminId, token);
                    if (!responseMessage.HasError)
                    {
                        isAuthorize = responseMessage.ResponseValue;
                    }
                }
                else
                {
                    if (userType == -102)
                    {
                        SessionManager.IsCustomer = false;
                        userId = (long)jData.FirstOrDefault(w => w.Key.Equals("AgentId")).Value;
                        userId = Core.Ghorahi.Service.Implementation.Authenticates.Scheduler.Authenticate.GetInventoryAgentId(userId, userType);
                    }
                    else if (userType == -101)
                    {
                        SessionManager.IsCustomer = true;
                        userId = (long)jData.FirstOrDefault(w => w.Key.Equals("CustomerId")).Value;
                    }
                    if (userId > 0 && !string.IsNullOrEmpty(token))
                    {
                        ResponseMessage<bool> responseMessage = Core.Ghorahi.Service.Implementation.Authenticates.Scheduler.Authenticate.VerifyToken(userId, token);
                        if (!responseMessage.HasError)
                        {
                            isAuthorize = responseMessage.ResponseValue;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            return isAuthorize;
        }
    }
}