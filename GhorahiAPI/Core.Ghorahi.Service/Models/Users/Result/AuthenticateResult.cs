﻿namespace Core.Ghorahi.Service.Models.Users.Result
{
    public class AuthenticateResult
    {
        public long CustomerId { get; set; }
        public string Name { get; set; }
        public long UserType { get; set; }
        public bool IsVerified { get; set; }
        public string ApiKey { get; set; }
        public long AgentId { get; set; }
        public bool HasSahabhagiScheme { get; set; }
        public string AdminId { get; set; }
    }
}