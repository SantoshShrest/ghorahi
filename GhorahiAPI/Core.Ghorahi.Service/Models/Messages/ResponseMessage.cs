﻿namespace Core.Ghorahi.Service.Models.Messages
{
    public class ResponseMessage<TValue>
    {
        public bool HasError { get; set; }
        public TValue ResponseValue { get; set; }
    }
}