﻿using System;
using System.Collections.Generic;

namespace Core.Ghorahi.Service.Models.Sales
{
    public class SalesOrderDTOs
    {
        public SalesOrderDTOs()
        {
            SalesOrderDetails = new List<SalesOrderDetailDTOs>();
        }
        public long ID { get; set; }
        public long SalesOrderTypeID { get; set; } = -106;
        public string Code { get; set; }
        public string OrderDate { get; set; }
        public string ValidUptoDatestamp { get; set; } = null;
        public long CustomerID { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPONumber { get; set; } = null;
        public string CustomerPODate { get; set; } = null;
        public string RevisionDatestamp { get; set; } = null;
        public string RevisionNumber { get; set; } = null;
        public long CompanyBranchLocID { get; set; } = -1;
        public bool? IsMadeToOrder { get; set; } = false;
        public long AgainstFormID { get; set; } = -1;
        public long SalesLedgerID { get; set; } = -116;
        public string BillingAddressCode { get; set; }
        public string DeliveryAddressCode { get; set; }
        public Nullable<bool> IsPartialShipment { get; set; } = false;
        public string ShipmentLocation { get; set; }
        public long ShipmentAgentID { get; set; }
        public long SalesPersonID { get; set; } = -1;
        public long ModeOfPaymentID { get; set; } = -102;
        public long PaymentTermID { get; set; } = -1;
        public long CurrencyID { get; set; } = -102;
        public decimal? CurrencyXRate { get; set; } = 0;
        public decimal? AdvanceAmount { get; set; } = 0;
        public string AdvanceRemarks { get; set; } = null;
        public long AdvanceVoucherID { get; set; } = -1;
        public bool? IsCompanyVehicle { get; set; } = false;
        public long CompanyVehicleAssetIndividualID { get; set; } = -1;
        public long DriverEmployeeID { get; set; } = -1;
        public long TransporterID { get; set; } = -1;
        public string TransporterName { get; set; } = null;
        public string VehicleNumber { get; set; } = null;
        public string VehicleType { get; set; } = null;
        public string DriverName { get; set; } = null;
        public string DriverMobileNumber { get; set; } = null;
        public string LicenseNumber { get; set; } = null;
        public decimal? NetWeight { get; set; } = 0;
        public decimal? GrossWeight { get; set; } = 0;
        public long UnitID { get; set; } = -1;
        public int? TotalNumberOfPackings { get; set; } = 0;
        public long DeclarationID { get; set; } = -101;
        public long ResponsibleEmployeeID { get; set; } = -1;
        public long StoreInchargeEmployeeID { get; set; } = -1;
        public long DispatchedByEmployeeID { get; set; } = -1;
        public long CheckByEmployeeID { get; set; } = -1;
        public string LastModifDatestamp { get; set; }
        public string CustomerRemarks { get; set; } = null;
        public string Remarks { get; set; } = null;
        public string TermsConditions { get; set; } = null;
        public string OtherConditions { get; set; } = null;
        public ICollection<SalesOrderDetailDTOs> SalesOrderDetails { get; set; }
    }
}