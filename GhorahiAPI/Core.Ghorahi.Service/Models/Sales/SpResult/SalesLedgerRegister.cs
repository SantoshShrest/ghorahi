﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Service.Models.Sales.SpResult
{
    public class SalesLedgerRegister
    {
        public string DateStamp { get; set; }
        public string SourceInv { get; set; }
        public string VoucherNo { get; set; }
        public long InvoiceID { get; set; }
        public string Particulars { get; set; }
        public string Narration { get; set; }
        public string ShortNarration { get; set; }
        public Nullable<decimal> Dr_Amt { get; set; }
        public Nullable<decimal> Cr_Amt { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string DR_CR { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeDate { get; set; }
        public string DateForSorting { get; set; }
    }
}
