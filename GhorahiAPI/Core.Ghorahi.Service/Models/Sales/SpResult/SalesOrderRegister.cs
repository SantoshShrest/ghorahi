﻿namespace Core.Ghorahi.Service.Models.Sales.SpResult
{
    public class SalesOrderRegister
    {
        public string SO_No_ { get; set; }
        public string SO_Date { get; set; }
        public string SO_Valid_Upto { get; set; }
        public string SO_Type { get; set; }
        public string Order_Status { get; set; }
        public string Customer { get; set; }
        public string Customer_Billing_Address { get; set; }
        public string Customer_Delivery_Address { get; set; }
        public string Sales_Tax_Form { get; set; }
        public string Agent { get; set; }
        public string Sales_Person { get; set; }
        public string Shipment_Port { get; set; }
        public string Payment_Terms { get; set; }
        public string MOP { get; set; }
        public string Currency { get; set; }
        public decimal? Exchange_Rate { get; set; }
        public decimal? Advance { get; set; }
        public string Advance_Remarks { get; set; }
        public string Prepd_By { get; set; }
        public string Verified_By { get; set; }
        public string Apprd_By { get; set; }
        public string Remarks { get; set; }
        public string Invoice_No_ { get; set; }
        public string Invoice_Date { get; set; }
        public int? Invoice_Line_No_ { get; set; }
        public string Stock_Item { get; set; }
        public decimal? SO_Qty { get; set; }
        public string Short_Remarks { get; set; }
        public string Delivery_Date { get; set; }
        public string MOT { get; set; }
        public string Priority { get; set; }
        public decimal? Rate { get; set; }
        public decimal? Line_Amt_ { get; set; }
        public decimal? Discount___ { get; set; }
        public decimal? Discount { get; set; }
        public decimal? CED_ST { get; set; }
        public decimal? CED_ST___ { get; set; }
        public decimal? Edu_Cess { get; set; }
        public decimal? Edu_Cess___ { get; set; }
        public decimal? SHS_EduCess { get; set; }
        public decimal? SHS_EduCess___ { get; set; }
        public decimal? Freight__Lc_ { get; set; }
        public decimal? Freight__Lc____ { get; set; }
        public decimal? Freight__Ex_ { get; set; }
        public decimal? Freight__Ex____ { get; set; }
        public decimal? Transit_Ins_ { get; set; }
        public decimal? Transit_Ins____ { get; set; }
        public decimal? Packing { get; set; }
        public decimal? Packing___ { get; set; }
        public decimal? Un_Loading { get; set; }
        public decimal? Un_Loading___ { get; set; }
        public decimal? Misc_Exp { get; set; }
        public decimal? Misc_Exp___ { get; set; }
        public decimal? Sales_Tax { get; set; }
        public decimal? Sales_Tax___ { get; set; }
        public decimal? CST { get; set; }
        public decimal? CST___ { get; set; }
        public decimal? Bill_Amt { get; set; }
    }
}