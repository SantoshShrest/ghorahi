﻿using System.Collections.Generic;

namespace Core.Ghorahi.Service.Models.Sales.Request
{
    public class SalesOrderRequest
    {
        public string OrderDate { get; set; }
        public long CustomerId { get; set; }
        public long AgentId { get; set; }
        public string AdminId { get; set; }
        public List<SalesOrderDetials> SalesOrderDetails { get; set; }
    }

    public class SalesOrderDetials
    {
        public long StockItemId { get; set; }
        public decimal Qty { get; set; }
        public decimal Rate { get; set; }
        public decimal Discount { get; set; }
        public decimal ServiceTax { get; set; }
        public decimal Vat { get; set; }
        public long OrderStatusId { get; set; }
    }
}