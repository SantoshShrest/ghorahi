﻿namespace Core.Ghorahi.Service.Models.Sales
{
    public class SalesDispatchRegisterRequest
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long? CustomerId { get; set; }
        public long? AgentId { get; set; }
        public string AdminId { get; set; }
        public bool IsAll { get; set; }
    }
}