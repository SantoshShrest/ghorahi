﻿namespace Core.Ghorahi.Service.Models.Sales.Request
{
    public class SalesLedgerRequest
    {
        public long AccountGroupId { get; set; } = 0;
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long? CustomerId { get; set; }
        public long AgentId { get; set; }
        public string AdminId { get; set; }
    }
}