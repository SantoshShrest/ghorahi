﻿namespace Core.Ghorahi.Service.Models.Sales.Request
{
    public class HoldSalesOrder
    {
        public string AdminId { get; set; }
        public long AgentId { get; set; }
        public long CustomerId { get; set; }
        public bool IsAll { get; set; }
        public long OrderStatusId { get; set; } = -107;
    }
}