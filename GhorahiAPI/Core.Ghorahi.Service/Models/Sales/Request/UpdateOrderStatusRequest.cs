﻿namespace Core.Ghorahi.Service.Models.Sales.Request
{
    public class UpdateOrderStatusRequest
    {
        public string AdminId { get; set; }
        public long AgentId { get; set; }
        public long CustomerId { get; set; }
        public long SalesOrderId { get; set; }
        public long SalesOrderDetailsId { get; set; }
        public long OrderStatusId { get; set; }
    }
}