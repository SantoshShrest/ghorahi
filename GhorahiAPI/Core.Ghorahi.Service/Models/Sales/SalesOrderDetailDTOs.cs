﻿namespace Core.Ghorahi.Service.Models.Sales
{
    public class SalesOrderDetailDTOs
    {
        public long ID { get; set; }
        public long SalesOrderID { get; set; }
        public int? LineNumber { get; set; }
        public long StockItemID { get; set; }
        public string StockItemName { get; set; }
        public long SubstitutionStockItemID { get; set; } = -1;
        public decimal? OrderQty { get; set; } = 0;
        public decimal? PlannedQty { get; set; } = 0;
        public long WorkOrderLocID { get; set; } = -1;
        public long DepartmentID { get; set; } = -1;
        public decimal? SalesPersonCommissionAmount { get; set; } = 0;
        public string DeliveryDate { get; set; } = " ";
        public string PlannedDeliveryDate { get; set; } = " ";
        public string PlannedShipmentDate { get; set; } = " ";
        public string PlannedInspectionDate { get; set; } = " ";
        public long ModeOfTransportID { get; set; } = -1;
        public long PriorityID { get; set; } = -1;
        public string InspectionDetails { get; set; } = " ";
        public string MasterPackagingName { get; set; } = " ";
        public int? MasterPackagingQty { get; set; } = 0;
        public bool? IsFinalInspectionComplete { get; set; } = false;
        public decimal? QtyToInvoice { get; set; } = 0;
        public decimal? QtyToShip { get; set; } = 0;
        public decimal? Qty { get; set; }
        public decimal? Rate { get; set; } = 0;
        public decimal? Amount { get; set; } = 0;
        public decimal? DiscountedRate { get; set; }
        public decimal? LineAmount { get; set; }
        public long InvoiceDiscountLedgerID { get; set; } = -1;
        public decimal? InvoiceDiscount { get; set; }
        public long ExciseOrServiceTaxLedgerID { get; set; } = -1;
        public decimal? ExciseOrServiceTax { get; set; }
        public long ServiceTaxEducationCessLedgerID { get; set; } = -1;
        public decimal? ServiceTaxEducationCess { get; set; } = 0;
        public long ServiceTaxSHSEducationCessLedgerID { get; set; } = -1;
        public decimal? ServiceTaxSHSEducationCess { get; set; } = 0;
        public long FreightLocalLedgerID { get; set; } = -1;
        public decimal? FreightLocal { get; set; } = 0;
        public long FreightEximLedgerID { get; set; } = -1;
        public decimal? FreightExim { get; set; } = 0;
        public long TransitInsuranceLedgerID { get; set; } = -1;
        public decimal? TransitInsurance { get; set; } = 0;
        public long PackingChargesLedgerID { get; set; } = -1;
        public decimal? PackingCharges { get; set; } = 0;
        public long LoadingUnloadingLedgerID { get; set; } = -1;
        public decimal? LoadingUnloading { get; set; } = 0;
        public long MiscExpLedgerID { get; set; } = -1;
        public decimal? MiscExp { get; set; } = 0;
        public long SalesTaxLedgerID { get; set; } = -1;
        public decimal? SalesTax { get; set; }
        public long CentralSalesTaxLedgerID { get; set; } = -1;
        public decimal? CentralSalesTax { get; set; } = 0;
        public decimal? TotalBillAmt { get; set; }
        public long FreightLocalNonRecvLedgerID { get; set; } = -1;
        public decimal? FreightLocalNonRecv { get; set; } = 0;
        public long FreightEximNonRecvLedgerID { get; set; } = -1;
        public decimal? FreightEximNonRecv { get; set; } = 0;
        public long TransitInsuranceNonRecvLedgerID { get; set; } = -1;
        public decimal? TransitInsuranceNonRecv { get; set; } = 0;
        public long PackingChargesNonRecvLedgerID { get; set; } = -1;
        public decimal? PackingChargesNonRecv { get; set; } = 0;
        public long LoadingUnloadingNonRecvLedgerID { get; set; } = -1;
        public decimal? LoadingUnloadingNonRecv { get; set; } = 0;
        public long MiscExpNonRecvLedgerID { get; set; } = -1;
        public decimal? MiscExpNonRecv { get; set; } = 0;
        public long SalesCommissionNonRecvLedgerID { get; set; } = -1;
        public decimal? SalesCommissionNonRecv { get; set; } = 0;
        public long CustomDutyBasicNonRecvLedgerID { get; set; } = -1;
        public decimal? CustomDutyBasicNonRecv { get; set; } = 0;
        public long AdditionalCustomDutyNonRecvLedgerID { get; set; } = -1;
        public decimal? AdditionalCustomDutyNonRecv { get; set; } = 0;
        public long SpecialAdditionalCustomDutyNonRecvLedgerID { get; set; } = -1;
        public decimal? SpecialAdditionalCustomDutyNonRecv { get; set; } = 0;
        public long AntiDumpingDutyNonRecvLedgerID { get; set; } = -1;
        public decimal? AntiDumpingDutyNonRecv { get; set; } = 0;
        public long ImportVATNonRecvLedgerID { get; set; } = -1;
        public decimal? ImportVATNonRecv { get; set; } = 0;
        public long ImportExiseOrServiceTaxNonRecvLedgerID { get; set; } = -1;
        public decimal? ImportExiseOrServiceTaxNonRecv { get; set; } = 0;
        public long AgentChargesNonRecvLedgerID { get; set; } = -1;
        public decimal? AgentChargesNonRecv { get; set; } = 0;
        public long LocID { get; set; } = -1;
        public bool? IsExciseonQty { get; set; } = false;
        public long OrderStatusID { get; set; } = -101;//-107 For Customer | -101 For Agent
        public string Remarks { get; set; } = " ";
        public SalesOrderDTOs SalesOrder { get; set; }
    }
}