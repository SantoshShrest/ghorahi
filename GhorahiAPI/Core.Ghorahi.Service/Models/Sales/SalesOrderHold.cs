﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Service.Models.Sales
{
    public class SalesOrderHold
    {
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public long AgentId { get; set; }
        public string AgentName { get; set; }
        public long SaleOrderId { get; set; }
        public long SaleOrderDetailId { get; set; }
        public long StockItemId { get; set; }
        public string StockItemName { get; set; }
        public decimal? Qty { get; set; }
        public long OrderStatusId { get; set; }
    }
}