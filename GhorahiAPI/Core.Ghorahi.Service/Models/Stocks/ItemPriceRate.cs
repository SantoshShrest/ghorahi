﻿namespace Core.Ghorahi.Service.Models.Stocks
{
    public class ItemPriceRate
    {
        public long StockItemId { get; set; }
        public long PartyId { get; set; }
        public decimal Rate { get; set; }
        public decimal Discount { get; set; }
        public decimal Vat { get; set; }
        public decimal ServiceTax { get; set; }
        public decimal ServiceTaxEC { get; set; }
        public decimal ServiceTaxSHEC { get; set; }
    }
}