﻿namespace Core.Ghorahi.Service.Models.Stocks.Request
{
    public class ItemPriceRateRequest
    {
        public long StockItemId { get; set; }
        public long PartyId { get; set; }
    }
}