﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Service.Models.Customers.Request
{
    public class CustomerNotificationRequest
    {
        public long CustomerId { get; set; }
    }
}