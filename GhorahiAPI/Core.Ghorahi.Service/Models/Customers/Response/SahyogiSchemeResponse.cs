﻿using Core.Ghorahi.Service.Models.Customers.Response;
using System.Collections.Generic;

namespace Core.Ghorahi.Service.Models.Customers
{
    public class SahyogiSchemeResponse
    {
        public int CurrentCategory { get; set; }
        public decimal PointsEarned { get; set; }
        public int NextCategory { get; set; }
        public double PointsRequired { get; set; }
        public double TotalDiscountedBags { get; set; }
        public double UsedDiscountedBags { get; set; }
        public double RemainingDiscountedBags { get; set; }
        public string DiscountPerBag { get; set; }
        //public List<SahyogiSchemeDetailDTOs> SahyogiSchemeDetails { get; set; }
        //public List<SahyogiSchemeFacilityDTOs> SahyogiSchemeFacilities { get; set; }
        //public List<SalesInvoiceResult> DiscountedBagsDetails { get; set; }
    }
}