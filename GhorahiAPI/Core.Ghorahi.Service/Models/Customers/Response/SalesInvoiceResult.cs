﻿namespace Core.Ghorahi.Service.Models.Customers.Response
{
    public class SalesInvoiceResult
    {
        public string InvoiceNo { get; set; }
        public decimal Quantity { get; set; }
        public decimal Discount { get; set; }
        public decimal Amount { get; set; }
        public string Date { get; set; }
    }
}