﻿namespace Core.Ghorahi.Service.Models.Customers
{
    public class SahabhagiSchemeFacilityDTOs
    {
        public int FacilityId { get; set; }
        public string Facilities { get; set; }
        public string Makalu { get; set; }
        public string Lhotse { get; set; }
        public string Kanchanjunga { get; set; }
        public string K2 { get; set; }
        public string Sagarmatha { get; set; }
        public bool Active { get; set; }
        public int CreatedBy { get; set; }
        public string CreationDatestamp { get; set; }
    }
}