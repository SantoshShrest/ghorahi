﻿namespace Core.Ghorahi.Service.Models
{
    public class SelectItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}