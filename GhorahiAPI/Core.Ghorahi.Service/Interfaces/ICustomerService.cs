﻿using Core.Ghorahi.Service.Models.Customers;
using Core.Ghorahi.Service.Models.Customers.Response;
using Core.Ghorahi.Service.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Service.Interfaces
{
    public interface ICustomerService
    {
        ResponseMessage<SahyogiSchemeResponse> GetSahyogiScheme(long customerId);
        ResponseMessage<List<SahabhagiSchemeDetailDTOs>> GetSahabhagiSchemeDetails(long customerId);
        ResponseMessage<List<SahabhagiSchemeFacilityDTOs>> GetSahabhagiSchemeFacilities(long customerId);
        ResponseMessage<List<SalesInvoiceResult>> GetDiscountedBags(long customerId);
        ResponseMessage<List<CustomerNotificationResponse>> GetDispatchOrderNotice(long customerId);
        ResponseMessage<List<CustomerNotificationResponse>> GetRegisterOrderNotice(long customerId);
    }
}