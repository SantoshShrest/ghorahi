﻿using Core.Ghorahi.Service.Models.Messages;
using Core.Ghorahi.Service.Models;
using System.Collections.Generic;
using Core.Ghorahi.Service.Models.Stocks;
using Core.Ghorahi.Service.Models.Stocks.Request;

namespace Core.Ghorahi.Service.Interfaces
{
    public interface IStockService
    {
        ResponseMessage<List<SelectItem>> GetStockItems();
        ResponseMessage<ItemPriceRate> GetStockItemPriceRate(ItemPriceRateRequest itemPriceRateRequest);
    }
}