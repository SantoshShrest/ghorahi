﻿using Core.Ghorahi.Service.Models.Messages;
using Core.Ghorahi.Service.Models.Users;
using Core.Ghorahi.Service.Models.Users.Result;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Service.Interfaces
{
    public interface IAuthenticateService
    {
        ResponseMessage<AuthenticateResult> VerifyUser(AuthenticateUser authenticateUser);
    }
}