﻿using Core.Ghorahi.Service.Models;
using Core.Ghorahi.Service.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Service.Interfaces
{
    public interface IAgentService
    {
        ResponseMessage<List<SelectItem>> GetCustomersByAgent(long agentId);
        ResponseMessage<List<SelectItem>> GetAgents();
    }
}