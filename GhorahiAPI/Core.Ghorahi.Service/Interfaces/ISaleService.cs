﻿using Core.Ghorahi.Service.Models;
using Core.Ghorahi.Service.Models.Messages;
using Core.Ghorahi.Service.Models.Sales;
using Core.Ghorahi.Service.Models.Sales.Request;
using Core.Ghorahi.Service.Models.Sales.SpResult;
using System.Collections.Generic;
using System.Linq;

namespace Core.Ghorahi.Service.Interfaces
{
    public interface ISaleService
    {
        ResponseMessage<int> SaveSalesOrder(SalesOrderRequest salesOrderRequest);
        ResponseMessage<List<SalesOrderRegister>> GetSalesOrdersRegister(SalesOrderRegisterRequest orderRequest);
        ResponseMessage<List<SalesDispatchRegister>> GetSalesDispatchRegister(SalesDispatchRegisterRequest dispatchRequest);
        ResponseMessage<List<SalesInvoiceRegister>> GetSalesInvoiceRegister(SalesInvoiceRegisterRequest invoiceRequest);
        ResponseMessage<List<SalesLedgerRegister>> GetSalesLedgerRegister(SalesLedgerRequest salesLedgerRequest);
        ResponseMessage<List<SalesOrderHold>> GetHoldSalesOrder(HoldSalesOrder holdSalesOrder);
        ResponseMessage<int> UpdateOrderStatus(UpdateOrderStatusRequest updateOrderStatus);
    }
}