﻿using Core.Ghorahi.Service.Models.Sales.SpResult;
using Core.Ghorahi.Data.Entity;
using System;
using Core.Ghorahi.Service.Models.Sales;
using System.Collections.Generic;
using System.Linq;

namespace Core.Ghorahi.Service.Converter
{
    public class SalesConverter
    {
        public static SalesOrderRegister ConvertToModel(usp_x_GetSalesOrderRegister_Result dbSalesOrderRegister, SalesOrderRegister salesOrderRegister)
        {
            try
            {
                salesOrderRegister.SO_No_ = dbSalesOrderRegister.SO_No_;
                salesOrderRegister.SO_Date = dbSalesOrderRegister.SO_Date;
                salesOrderRegister.SO_Valid_Upto = dbSalesOrderRegister.SO_Valid_Upto;
                salesOrderRegister.SO_Type = dbSalesOrderRegister.SO_Type;
                salesOrderRegister.Order_Status = dbSalesOrderRegister.Order_Status;
                salesOrderRegister.Customer = dbSalesOrderRegister.Customer;
                salesOrderRegister.Customer_Billing_Address = dbSalesOrderRegister.Customer_Billing_Address;
                salesOrderRegister.Customer_Delivery_Address = dbSalesOrderRegister.Customer_Delivery_Address;
                //salesOrderRegister.Sales_Tax_Form = dbSalesOrderRegister.Sales_Tax_Form;
                salesOrderRegister.Agent = dbSalesOrderRegister.Agent;
                salesOrderRegister.Sales_Person = dbSalesOrderRegister.Sales_Person;
                salesOrderRegister.Shipment_Port = dbSalesOrderRegister.Shipment_Port;
                salesOrderRegister.Payment_Terms = dbSalesOrderRegister.Payment_Terms;
                salesOrderRegister.MOP = dbSalesOrderRegister.MOP;
                salesOrderRegister.Currency = dbSalesOrderRegister.Currency;
                salesOrderRegister.Exchange_Rate = dbSalesOrderRegister.Exchange_Rate;
                salesOrderRegister.Advance = dbSalesOrderRegister.Advance;
                salesOrderRegister.Advance_Remarks = dbSalesOrderRegister.Advance_Remarks;
                salesOrderRegister.Prepd_By = dbSalesOrderRegister.Prepd_By;
                salesOrderRegister.Verified_By = dbSalesOrderRegister.Verified_By;
                salesOrderRegister.Apprd_By = dbSalesOrderRegister.Apprd_By;
                salesOrderRegister.Remarks = dbSalesOrderRegister.Remarks;
                salesOrderRegister.Invoice_No_ = dbSalesOrderRegister.Invoice_No_;
                salesOrderRegister.Invoice_Date = dbSalesOrderRegister.Invoice_Date;
                salesOrderRegister.Invoice_Line_No_ = dbSalesOrderRegister.Invoice_Line_No_;
                salesOrderRegister.Stock_Item = dbSalesOrderRegister.Stock_Item;
                salesOrderRegister.SO_Qty = dbSalesOrderRegister.SO_Qty;
                salesOrderRegister.Short_Remarks = dbSalesOrderRegister.Short_Remarks;
                salesOrderRegister.Delivery_Date = dbSalesOrderRegister.Delivery_Date;
                salesOrderRegister.MOT = dbSalesOrderRegister.MOT;
                salesOrderRegister.Priority = dbSalesOrderRegister.Priority;
                salesOrderRegister.Rate = dbSalesOrderRegister.Rate;
                salesOrderRegister.Line_Amt_ = decimal.Parse(((decimal)dbSalesOrderRegister.Line_Amt_).ToString("F"));
                salesOrderRegister.Discount = dbSalesOrderRegister.Discount;
                salesOrderRegister.CED_ST = dbSalesOrderRegister.CED_ST;
                salesOrderRegister.CED_ST___ = dbSalesOrderRegister.CED_ST___;
                salesOrderRegister.Edu_Cess = dbSalesOrderRegister.Edu_Cess;
                salesOrderRegister.Edu_Cess___ = dbSalesOrderRegister.Edu_Cess___;
                salesOrderRegister.SHS_EduCess = dbSalesOrderRegister.SHS_EduCess;
                salesOrderRegister.SHS_EduCess___ = dbSalesOrderRegister.SHS_EduCess___;
                salesOrderRegister.Freight__Lc_ = dbSalesOrderRegister.Freight__Lc_;
                salesOrderRegister.Freight__Lc____ = dbSalesOrderRegister.Freight__Lc____;
                salesOrderRegister.Freight__Ex_ = dbSalesOrderRegister.Freight__Ex_;
                salesOrderRegister.Freight__Ex____ = dbSalesOrderRegister.Freight__Ex____;
                salesOrderRegister.Transit_Ins_ = dbSalesOrderRegister.Transit_Ins_;
                salesOrderRegister.Transit_Ins____ = dbSalesOrderRegister.Transit_Ins____;
                salesOrderRegister.Packing = dbSalesOrderRegister.Packing;
                salesOrderRegister.Packing___ = dbSalesOrderRegister.Packing___;
                salesOrderRegister.Un_Loading = dbSalesOrderRegister.Un_Loading;
                salesOrderRegister.Un_Loading___ = dbSalesOrderRegister.Un_Loading___;
                salesOrderRegister.Misc_Exp = dbSalesOrderRegister.Misc_Exp;
                salesOrderRegister.Misc_Exp___ = dbSalesOrderRegister.Misc_Exp___;
                salesOrderRegister.Sales_Tax = decimal.Parse(((decimal)dbSalesOrderRegister.Sales_Tax).ToString("F"));
                salesOrderRegister.Sales_Tax___ = decimal.Parse(((decimal)dbSalesOrderRegister.Sales_Tax___).ToString("F"));
                salesOrderRegister.CST = dbSalesOrderRegister.CST;
                salesOrderRegister.CST___ = dbSalesOrderRegister.CST___;
                salesOrderRegister.Bill_Amt = decimal.Parse(((decimal)dbSalesOrderRegister.Bill_Amt).ToString("F"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return salesOrderRegister;
        }

        public static SalesDispatchRegister ConvertToModel(usp_x_GetSalesDispatchRegister_Result dbSalesDispatchRegister, SalesDispatchRegister salesDispatchRegister)
        {
            try
            {
                salesDispatchRegister.SD_No_ = dbSalesDispatchRegister.SD_No_;
                salesDispatchRegister.SD_Date = dbSalesDispatchRegister.SD_Date;
                salesDispatchRegister.SD_Valid_Upto = dbSalesDispatchRegister.SD_Valid_Upto;
                salesDispatchRegister.SD_Type = dbSalesDispatchRegister.SD_Type;
                salesDispatchRegister.Customer = dbSalesDispatchRegister.Customer;
                salesDispatchRegister.Customer_Billing_Address = dbSalesDispatchRegister.Customer_Billing_Address;
                salesDispatchRegister.Customer_Delivery_Address = dbSalesDispatchRegister.Customer_Delivery_Address;
                salesDispatchRegister.Sales_Tax_Form = dbSalesDispatchRegister.Sales_Tax_Form;
                salesDispatchRegister.Gross_Weight = dbSalesDispatchRegister.Gross_Weight;
                salesDispatchRegister.Net_Weight = dbSalesDispatchRegister.Net_Weight;
                salesDispatchRegister.Agent = dbSalesDispatchRegister.Agent;
                salesDispatchRegister.Order_Status = dbSalesDispatchRegister.Order_Status;
                salesDispatchRegister.Shipment_Port = dbSalesDispatchRegister.Shipment_Port;
                salesDispatchRegister.Payment_Terms = dbSalesDispatchRegister.Payment_Terms;
                salesDispatchRegister.MOP = dbSalesDispatchRegister.MOP;
                salesDispatchRegister.Currency = dbSalesDispatchRegister.Currency;
                salesDispatchRegister.Exchange_Rate = dbSalesDispatchRegister.Exchange_Rate;
                salesDispatchRegister.Advance = dbSalesDispatchRegister.Advance;
                salesDispatchRegister.Advance_Remarks = dbSalesDispatchRegister.Advance_Remarks;
                salesDispatchRegister.Prepd_By = dbSalesDispatchRegister.Prepd_By;
                salesDispatchRegister.Verified_By = dbSalesDispatchRegister.Verified_By;
                salesDispatchRegister.Apprd_By = dbSalesDispatchRegister.Apprd_By;
                salesDispatchRegister.Remarks = dbSalesDispatchRegister.Remarks;
                salesDispatchRegister.Transporter = dbSalesDispatchRegister.Transporter;
                salesDispatchRegister.VehicleNumber = dbSalesDispatchRegister.VehicleNumber;
                salesDispatchRegister.VehicleType = dbSalesDispatchRegister.VehicleType;
                salesDispatchRegister.DriverName = dbSalesDispatchRegister.DriverName;
                salesDispatchRegister.DriverMobileNumber = dbSalesDispatchRegister.DriverMobileNumber;
                salesDispatchRegister.LicenseNumber = dbSalesDispatchRegister.LicenseNumber;
                salesDispatchRegister.Invoice_No_ = dbSalesDispatchRegister.Invoice_No_;
                salesDispatchRegister.Invoice_Date = dbSalesDispatchRegister.Invoice_Date;
                salesDispatchRegister.Invoice_Line_No_ = dbSalesDispatchRegister.Invoice_Line_No_;
                salesDispatchRegister.Stock_Item = dbSalesDispatchRegister.Stock_Item;
                salesDispatchRegister.SD_Qty = dbSalesDispatchRegister.SD_Qty;
                salesDispatchRegister.Short_Remarks = dbSalesDispatchRegister.Short_Remarks;
                salesDispatchRegister.Delivery_Date = dbSalesDispatchRegister.Delivery_Date;
                salesDispatchRegister.MOT = dbSalesDispatchRegister.MOT;
                salesDispatchRegister.Priority = dbSalesDispatchRegister.Priority;
                salesDispatchRegister.Rate = dbSalesDispatchRegister.Rate;
                salesDispatchRegister.Line_Amt_ = decimal.Parse(((decimal)dbSalesDispatchRegister.Line_Amt_).ToString("F"));
                salesDispatchRegister.Discount = dbSalesDispatchRegister.Discount;
                salesDispatchRegister.Discount___ = dbSalesDispatchRegister.Discount___;
                salesDispatchRegister.CED_ST = dbSalesDispatchRegister.CED_ST;
                salesDispatchRegister.CED_ST___ = dbSalesDispatchRegister.CED_ST___;
                salesDispatchRegister.Edu_Cess = dbSalesDispatchRegister.Edu_Cess;
                salesDispatchRegister.Edu_Cess___ = dbSalesDispatchRegister.Edu_Cess___;
                salesDispatchRegister.SHS_EduCess = dbSalesDispatchRegister.SHS_EduCess;
                salesDispatchRegister.SHS_EduCess___ = dbSalesDispatchRegister.SHS_EduCess___;
                salesDispatchRegister.Freight__Lc_ = dbSalesDispatchRegister.Freight__Lc_;
                salesDispatchRegister.Freight__Lc____ = dbSalesDispatchRegister.Freight__Lc____;
                salesDispatchRegister.Freight__Ex_ = dbSalesDispatchRegister.Freight__Ex_;
                salesDispatchRegister.Freight__Ex____ = dbSalesDispatchRegister.Freight__Ex____;
                salesDispatchRegister.Transit_Ins_ = dbSalesDispatchRegister.Transit_Ins_;
                salesDispatchRegister.Transit_Ins____ = dbSalesDispatchRegister.Transit_Ins____;
                salesDispatchRegister.Packing = dbSalesDispatchRegister.Packing;
                salesDispatchRegister.Packing___ = dbSalesDispatchRegister.Packing___;
                salesDispatchRegister.Un_Loading = dbSalesDispatchRegister.Un_Loading;
                salesDispatchRegister.Un_Loading___ = dbSalesDispatchRegister.Un_Loading___;
                salesDispatchRegister.Misc_Exp = dbSalesDispatchRegister.Misc_Exp;
                salesDispatchRegister.Misc_Exp___ = dbSalesDispatchRegister.Misc_Exp___;
                salesDispatchRegister.VAT = dbSalesDispatchRegister.VAT;
                salesDispatchRegister.VAT___ = dbSalesDispatchRegister.VAT___;
                salesDispatchRegister.CST = dbSalesDispatchRegister.CST;
                salesDispatchRegister.CST___ = dbSalesDispatchRegister.CST___;
                salesDispatchRegister.Bill_Amt = decimal.Parse(((decimal)dbSalesDispatchRegister.Bill_Amt).ToString("F"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return salesDispatchRegister;
        }

        public static SalesInvoiceRegister ConvertToModel(usp_x_GetSalesInvoiceRegister_Result dbSalesInvoiceRegister, SalesInvoiceRegister salesInvoiceRegister)
        {
            try
            {
                salesInvoiceRegister.Invoice_No_ = dbSalesInvoiceRegister.Invoice_No_;
                salesInvoiceRegister.Invoice_Date = dbSalesInvoiceRegister.Invoice_Date;
                salesInvoiceRegister.Invoice_Valid_Upto = dbSalesInvoiceRegister.Invoice_Valid_Upto;
                salesInvoiceRegister.Invoice_Type = dbSalesInvoiceRegister.Invoice_Type;
                salesInvoiceRegister.Customer = dbSalesInvoiceRegister.Customer;
                salesInvoiceRegister.VAT_No = dbSalesInvoiceRegister.VAT_No;
                salesInvoiceRegister.Customer_Billing_Address = dbSalesInvoiceRegister.Customer_Billing_Address;
                salesInvoiceRegister.Customer_Delivery_Address = dbSalesInvoiceRegister.Customer_Delivery_Address;
                salesInvoiceRegister.Sales_Tax_Form = dbSalesInvoiceRegister.Sales_Tax_Form;
                salesInvoiceRegister.Agent = dbSalesInvoiceRegister.Agent;
                salesInvoiceRegister.Sales_Person = dbSalesInvoiceRegister.Sales_Person;
                salesInvoiceRegister.Shipment_Port = dbSalesInvoiceRegister.Shipment_Port;
                salesInvoiceRegister.Payment_Terms = dbSalesInvoiceRegister.Payment_Terms;
                salesInvoiceRegister.MOP = dbSalesInvoiceRegister.MOP;
                salesInvoiceRegister.Currency = dbSalesInvoiceRegister.Currency;
                salesInvoiceRegister.Exchange_Rate = dbSalesInvoiceRegister.Exchange_Rate;
                salesInvoiceRegister.Advance = dbSalesInvoiceRegister.Advance;
                salesInvoiceRegister.Advance_Remarks = dbSalesInvoiceRegister.Advance_Remarks;
                salesInvoiceRegister.Prepd_By = dbSalesInvoiceRegister.Prepd_By;
                salesInvoiceRegister.Verified_By = dbSalesInvoiceRegister.Verified_By;
                salesInvoiceRegister.Apprd_By = dbSalesInvoiceRegister.Apprd_By;
                salesInvoiceRegister.Remarks = dbSalesInvoiceRegister.Remarks;
                salesInvoiceRegister.SO_No_ = dbSalesInvoiceRegister.SO_No_;
                salesInvoiceRegister.SO_Date = dbSalesInvoiceRegister.SO_Date;
                salesInvoiceRegister.SO_Line_No_ = dbSalesInvoiceRegister.SO_Line_No_;
                salesInvoiceRegister.Stock_Item = dbSalesInvoiceRegister.Stock_Item;
                salesInvoiceRegister.Invoice_Qty = dbSalesInvoiceRegister.Invoice_Qty;
                salesInvoiceRegister.Short_Remarks = dbSalesInvoiceRegister.Short_Remarks;
                salesInvoiceRegister.Delivery_Date = dbSalesInvoiceRegister.Delivery_Date;
                salesInvoiceRegister.MOT = dbSalesInvoiceRegister.MOT;
                salesInvoiceRegister.Priority = dbSalesInvoiceRegister.Priority;
                salesInvoiceRegister.Rate = dbSalesInvoiceRegister.Rate;
                salesInvoiceRegister.Line_Amt_ = decimal.Parse(((decimal)dbSalesInvoiceRegister.Line_Amt_).ToString("F"));
                salesInvoiceRegister.Order_Status = dbSalesInvoiceRegister.Order_Status;
                salesInvoiceRegister.Discount = dbSalesInvoiceRegister.Discount;
                salesInvoiceRegister.Discount___ = dbSalesInvoiceRegister.Discount___;
                salesInvoiceRegister.CED_ST = dbSalesInvoiceRegister.CED_ST;
                salesInvoiceRegister.CED_ST___ = dbSalesInvoiceRegister.CED_ST___;
                salesInvoiceRegister.Edu_Cess = dbSalesInvoiceRegister.Edu_Cess;
                salesInvoiceRegister.Edu_Cess___ = dbSalesInvoiceRegister.Edu_Cess___;
                salesInvoiceRegister.SHS_EduCess = dbSalesInvoiceRegister.SHS_EduCess;
                salesInvoiceRegister.SHS_EduCess___ = dbSalesInvoiceRegister.SHS_EduCess___;
                salesInvoiceRegister.Freight__Lc_ = dbSalesInvoiceRegister.Freight__Lc_;
                salesInvoiceRegister.Freight__Lc____ = dbSalesInvoiceRegister.Freight__Lc____;
                salesInvoiceRegister.Freight__Ex_ = dbSalesInvoiceRegister.Freight__Ex_;
                salesInvoiceRegister.Freight__Ex____ = dbSalesInvoiceRegister.Freight__Ex____;
                salesInvoiceRegister.Transit_Ins_ = dbSalesInvoiceRegister.Transit_Ins_;
                salesInvoiceRegister.Transit_Ins____ = dbSalesInvoiceRegister.Transit_Ins____;
                salesInvoiceRegister.Packing = dbSalesInvoiceRegister.Packing;
                salesInvoiceRegister.Packing___ = dbSalesInvoiceRegister.Packing___;
                salesInvoiceRegister.Un_Loading = dbSalesInvoiceRegister.Un_Loading;
                salesInvoiceRegister.Un_Loading___ = dbSalesInvoiceRegister.Un_Loading___;
                salesInvoiceRegister.Misc_Exp = dbSalesInvoiceRegister.Misc_Exp;
                salesInvoiceRegister.Misc_Exp___ = dbSalesInvoiceRegister.Misc_Exp___;
                salesInvoiceRegister.VAT = dbSalesInvoiceRegister.VAT;
                salesInvoiceRegister.VAT___ = dbSalesInvoiceRegister.VAT___;
                salesInvoiceRegister.CST = dbSalesInvoiceRegister.CST;
                salesInvoiceRegister.CST___ = dbSalesInvoiceRegister.CST___;
                salesInvoiceRegister.Bill_Amt = decimal.Parse(((decimal)dbSalesInvoiceRegister.Bill_Amt).ToString("F"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return salesInvoiceRegister;
        }

        public static SalesLedgerRegister ConvertToModel(usp_SingleLineLedgerViewReport_Result dbSalesLedgerRegister, SalesLedgerRegister salesLedgerRegister)
        {
            try
            {
                salesLedgerRegister.DateStamp = dbSalesLedgerRegister.DateStamp;
                salesLedgerRegister.SourceInv = dbSalesLedgerRegister.SourceInv;
                salesLedgerRegister.VoucherNo = dbSalesLedgerRegister.VoucherNo;
                salesLedgerRegister.InvoiceID = dbSalesLedgerRegister.InvoiceID;
                salesLedgerRegister.Particulars = dbSalesLedgerRegister.Particulars;
                salesLedgerRegister.Narration = dbSalesLedgerRegister.Narration;
                salesLedgerRegister.ShortNarration = dbSalesLedgerRegister.ShortNarration;
                salesLedgerRegister.Dr_Amt = dbSalesLedgerRegister.Dr_Amt;
                salesLedgerRegister.Cr_Amt = dbSalesLedgerRegister.Cr_Amt;
                salesLedgerRegister.Amount = dbSalesLedgerRegister.Amount;
                salesLedgerRegister.DR_CR = dbSalesLedgerRegister.DR_CR;
                salesLedgerRegister.ChequeNo = dbSalesLedgerRegister.ChequeNo;
                salesLedgerRegister.ChequeDate = dbSalesLedgerRegister.ChequeDate;
                salesLedgerRegister.DateForSorting = dbSalesLedgerRegister.DateForSorting;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return salesLedgerRegister;
        }

        public static SalesOrderDTOs ConvertToModel(SalesOrder dbSalesOrder, SalesOrderDTOs salesOrder)
        {
            try
            {
                salesOrder.ID = dbSalesOrder.ID;
                salesOrder.SalesOrderTypeID = dbSalesOrder.SalesOrderTypeID;
                salesOrder.Code = dbSalesOrder.Code;
                salesOrder.OrderDate = dbSalesOrder.OrderDate;
                salesOrder.ValidUptoDatestamp = dbSalesOrder.ValidUptoDatestamp;
                salesOrder.CustomerID = dbSalesOrder.CustomerID;
                salesOrder.CustomerName = dbSalesOrder.Customer.Name;
                salesOrder.CustomerPONumber = dbSalesOrder.CustomerPONumber;
                salesOrder.CustomerPODate = dbSalesOrder.CustomerPODate;
                salesOrder.RevisionDatestamp = dbSalesOrder.RevisionDatestamp;
                salesOrder.RevisionNumber = dbSalesOrder.RevisionNumber;
                salesOrder.CompanyBranchLocID = dbSalesOrder.CompanyBranchLocID;
                salesOrder.IsMadeToOrder = dbSalesOrder.IsMadeToOrder;
                salesOrder.AgainstFormID = dbSalesOrder.AgainstFormID;
                salesOrder.SalesLedgerID = dbSalesOrder.SalesLedgerID;
                salesOrder.BillingAddressCode = dbSalesOrder.BillingAddressCode;
                salesOrder.DeliveryAddressCode = dbSalesOrder.DeliveryAddressCode;
                salesOrder.IsPartialShipment = dbSalesOrder.IsPartialShipment;
                salesOrder.ShipmentLocation = dbSalesOrder.ShipmentLocation;
                salesOrder.ShipmentAgentID = dbSalesOrder.ShipmentAgentID;
                salesOrder.SalesPersonID = dbSalesOrder.SalesPersonID;
                salesOrder.ModeOfPaymentID = dbSalesOrder.ModeOfPaymentID;
                salesOrder.PaymentTermID = dbSalesOrder.PaymentTermID;
                salesOrder.CurrencyID = dbSalesOrder.CurrencyID;
                salesOrder.CurrencyXRate = dbSalesOrder.CurrencyXRate;
                salesOrder.AdvanceAmount = dbSalesOrder.AdvanceAmount;
                salesOrder.AdvanceRemarks = dbSalesOrder.AdvanceRemarks;
                salesOrder.AdvanceVoucherID = dbSalesOrder.AdvanceVoucherID;
                salesOrder.IsCompanyVehicle = dbSalesOrder.IsCompanyVehicle;
                salesOrder.CompanyVehicleAssetIndividualID = dbSalesOrder.CompanyVehicleAssetIndividualID;
                salesOrder.DriverEmployeeID = dbSalesOrder.DriverEmployeeID;
                salesOrder.TransporterID = dbSalesOrder.TransporterID;
                salesOrder.TransporterName = dbSalesOrder.TransporterName;
                salesOrder.VehicleNumber = dbSalesOrder.VehicleNumber;
                salesOrder.VehicleType = dbSalesOrder.VehicleType;
                salesOrder.DriverName = dbSalesOrder.DriverName;
                salesOrder.DriverMobileNumber = dbSalesOrder.DriverMobileNumber;
                salesOrder.LicenseNumber = dbSalesOrder.LicenseNumber;
                salesOrder.NetWeight = dbSalesOrder.NetWeight;
                salesOrder.GrossWeight = dbSalesOrder.GrossWeight;
                salesOrder.UnitID = dbSalesOrder.UnitID;
                salesOrder.TotalNumberOfPackings = dbSalesOrder.TotalNumberOfPackings;
                salesOrder.DeclarationID = dbSalesOrder.DeclarationID;
                salesOrder.ResponsibleEmployeeID = dbSalesOrder.ResponsibleEmployeeID;
                salesOrder.StoreInchargeEmployeeID = dbSalesOrder.StoreInchargeEmployeeID;
                salesOrder.DispatchedByEmployeeID = dbSalesOrder.DispatchedByEmployeeID;
                salesOrder.CheckByEmployeeID = dbSalesOrder.CheckByEmployeeID;
                salesOrder.LastModifDatestamp = dbSalesOrder.LastModifDatestamp;
                salesOrder.CustomerRemarks = dbSalesOrder.CustomerRemarks;
                salesOrder.Remarks = dbSalesOrder.Remarks;
                salesOrder.TermsConditions = dbSalesOrder.TermsConditions;
                salesOrder.OtherConditions = dbSalesOrder.OtherConditions;
                salesOrder.SalesOrderDetails = new List<SalesOrderDetailDTOs>();
                foreach (SalesOrderDetail dbOrderDetails in dbSalesOrder.SalesOrderDetails.Where(w => w.OrderStatusID.Equals(-107)))
                {
                    salesOrder.SalesOrderDetails.Add(new SalesOrderDetailDTOs
                    {
                        ID = dbOrderDetails.ID,
                        SalesOrderID = dbOrderDetails.SalesOrderID,
                        LineNumber = dbOrderDetails.LineNumber,
                        StockItemID = dbOrderDetails.StockItemID,
                        StockItemName = dbOrderDetails.StockItem.Name,
                        SubstitutionStockItemID = dbOrderDetails.SubstitutionStockItemID,
                        OrderQty = dbOrderDetails.OrderQty,
                        PlannedQty = dbOrderDetails.PlannedQty,
                        WorkOrderLocID = dbOrderDetails.WorkOrderLocID,
                        DepartmentID = dbOrderDetails.DepartmentID,
                        SalesPersonCommissionAmount = dbOrderDetails.SalesPersonCommissionAmount,
                        DeliveryDate = dbOrderDetails.DeliveryDate,
                        PlannedDeliveryDate = dbOrderDetails.PlannedDeliveryDate,
                        PlannedShipmentDate = dbOrderDetails.PlannedShipmentDate,
                        PlannedInspectionDate = dbOrderDetails.PlannedInspectionDate,
                        ModeOfTransportID = dbOrderDetails.ModeOfTransportID,
                        PriorityID = dbOrderDetails.PriorityID,
                        InspectionDetails = dbOrderDetails.InspectionDetails,
                        MasterPackagingName = dbOrderDetails.MasterPackagingName,
                        MasterPackagingQty = dbOrderDetails.MasterPackagingQty,
                        IsFinalInspectionComplete = dbOrderDetails.IsFinalInspectionComplete,
                        QtyToInvoice = dbOrderDetails.QtyToInvoice,
                        QtyToShip = dbOrderDetails.QtyToShip,
                        Qty = dbOrderDetails.Qty,
                        Rate = dbOrderDetails.Rate,
                        Amount = dbOrderDetails.Amount,
                        DiscountedRate = dbOrderDetails.DiscountedRate,
                        LineAmount = dbOrderDetails.LineAmount,
                        InvoiceDiscountLedgerID = dbOrderDetails.InvoiceDiscountLedgerID,
                        InvoiceDiscount = dbOrderDetails.InvoiceDiscount,
                        ExciseOrServiceTaxLedgerID = dbOrderDetails.ExciseOrServiceTaxLedgerID,
                        ExciseOrServiceTax = dbOrderDetails.ExciseOrServiceTax,
                        ServiceTaxEducationCessLedgerID = dbOrderDetails.ServiceTaxEducationCessLedgerID,
                        ServiceTaxEducationCess = dbOrderDetails.ServiceTaxEducationCess,
                        ServiceTaxSHSEducationCessLedgerID = dbOrderDetails.ServiceTaxSHSEducationCessLedgerID,
                        ServiceTaxSHSEducationCess = dbOrderDetails.ServiceTaxSHSEducationCess,
                        FreightLocalLedgerID = dbOrderDetails.FreightLocalLedgerID,
                        FreightLocal = dbOrderDetails.FreightLocal,
                        FreightEximLedgerID = dbOrderDetails.FreightEximLedgerID,
                        FreightExim = dbOrderDetails.FreightExim,
                        TransitInsuranceLedgerID = dbOrderDetails.TransitInsuranceLedgerID,
                        TransitInsurance = dbOrderDetails.TransitInsurance,
                        PackingChargesLedgerID = dbOrderDetails.PackingChargesLedgerID,
                        PackingCharges = dbOrderDetails.PackingCharges,
                        LoadingUnloadingLedgerID = dbOrderDetails.LoadingUnloadingLedgerID,
                        LoadingUnloading = dbOrderDetails.LoadingUnloading,
                        MiscExpLedgerID = dbOrderDetails.MiscExpLedgerID,
                        MiscExp = dbOrderDetails.MiscExp,
                        SalesTaxLedgerID = dbOrderDetails.SalesTaxLedgerID,
                        SalesTax = dbOrderDetails.SalesTax,
                        CentralSalesTaxLedgerID = dbOrderDetails.CentralSalesTaxLedgerID,
                        CentralSalesTax = dbOrderDetails.CentralSalesTax,
                        TotalBillAmt = dbOrderDetails.TotalBillAmt,
                        FreightLocalNonRecvLedgerID = dbOrderDetails.FreightLocalNonRecvLedgerID,
                        FreightLocalNonRecv = dbOrderDetails.FreightLocalNonRecv,
                        FreightEximNonRecvLedgerID = dbOrderDetails.FreightEximNonRecvLedgerID,
                        FreightEximNonRecv = dbOrderDetails.FreightEximNonRecv,
                        TransitInsuranceNonRecvLedgerID = dbOrderDetails.TransitInsuranceNonRecvLedgerID,
                        TransitInsuranceNonRecv = dbOrderDetails.TransitInsuranceNonRecv,
                        PackingChargesNonRecvLedgerID = dbOrderDetails.PackingChargesNonRecvLedgerID,
                        PackingChargesNonRecv = dbOrderDetails.PackingChargesNonRecv,
                        LoadingUnloadingNonRecvLedgerID = dbOrderDetails.LoadingUnloadingNonRecvLedgerID,
                        LoadingUnloadingNonRecv = dbOrderDetails.LoadingUnloadingNonRecv,
                        MiscExpNonRecvLedgerID = dbOrderDetails.MiscExpNonRecvLedgerID,
                        MiscExpNonRecv = dbOrderDetails.MiscExpNonRecv,
                        SalesCommissionNonRecvLedgerID = dbOrderDetails.SalesCommissionNonRecvLedgerID,
                        SalesCommissionNonRecv = dbOrderDetails.SalesCommissionNonRecv,
                        CustomDutyBasicNonRecv = dbOrderDetails.CustomDutyBasicNonRecv,
                        CustomDutyBasicNonRecvLedgerID = dbOrderDetails.CustomDutyBasicNonRecvLedgerID,
                        AdditionalCustomDutyNonRecvLedgerID = dbOrderDetails.AdditionalCustomDutyNonRecvLedgerID,
                        AdditionalCustomDutyNonRecv = dbOrderDetails.AdditionalCustomDutyNonRecv,
                        SpecialAdditionalCustomDutyNonRecvLedgerID = dbOrderDetails.SpecialAdditionalCustomDutyNonRecvLedgerID,
                        SpecialAdditionalCustomDutyNonRecv = dbOrderDetails.SpecialAdditionalCustomDutyNonRecv,
                        AntiDumpingDutyNonRecvLedgerID = dbOrderDetails.AntiDumpingDutyNonRecvLedgerID,
                        AntiDumpingDutyNonRecv = dbOrderDetails.AntiDumpingDutyNonRecv,
                        ImportVATNonRecvLedgerID = dbOrderDetails.ImportVATNonRecvLedgerID,
                        ImportVATNonRecv = dbOrderDetails.ImportVATNonRecv,
                        ImportExiseOrServiceTaxNonRecv = dbOrderDetails.ImportExiseOrServiceTaxNonRecv,
                        ImportExiseOrServiceTaxNonRecvLedgerID = dbOrderDetails.ImportExiseOrServiceTaxNonRecvLedgerID,
                        AgentChargesNonRecvLedgerID = dbOrderDetails.AgentChargesNonRecvLedgerID,
                        AgentChargesNonRecv = dbOrderDetails.AgentChargesNonRecv,
                        LocID = dbOrderDetails.LocID,
                        IsExciseonQty = dbOrderDetails.IsExciseonQty,
                        OrderStatusID = dbOrderDetails.OrderStatusID,
                        Remarks = dbOrderDetails.Remarks
                    });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return salesOrder;
        }

        public static SalesOrderHold ConvertToModel(SalesOrder dbSalesOrder, SalesOrderHold salesOrderHold)
        {
            try
            {
                foreach (SalesOrderDetail dbSalesOrderDetail in dbSalesOrder.SalesOrderDetails.Where(w => w.OrderStatusID.Equals(-107)))
                {
                    salesOrderHold.CustomerId = Core.Ghorahi.Service.Implementation.Agents.Scheduler.Agent.GetCustomerIdFromInventory(dbSalesOrder.CustomerID);
                    salesOrderHold.CustomerName = dbSalesOrder.Customer.Name;
                    salesOrderHold.AgentId = dbSalesOrder.ShipmentAgentID;
                    salesOrderHold.AgentName = dbSalesOrder.ShipmentAgent.CompanyName;
                    salesOrderHold.SaleOrderId = dbSalesOrder.ID;
                    salesOrderHold.SaleOrderDetailId = dbSalesOrderDetail.ID;
                    salesOrderHold.StockItemId = dbSalesOrderDetail.StockItemID;
                    salesOrderHold.StockItemName = dbSalesOrderDetail.StockItem.Name;
                    salesOrderHold.Qty = dbSalesOrderDetail.Qty;
                    salesOrderHold.OrderStatusId = dbSalesOrderDetail.OrderStatusID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return salesOrderHold;
        }

        public static SalesOrder ConvertToEntity(SalesOrderDTOs salesOrder, SalesOrder dbSalesOrder)
        {
            try
            {
                dbSalesOrder.ID = salesOrder.ID;
                dbSalesOrder.SalesOrderTypeID = salesOrder.SalesOrderTypeID;
                //dbSalesOrder.Code = salesOrder.Code;
                dbSalesOrder.OrderDate = salesOrder.OrderDate;
                dbSalesOrder.ValidUptoDatestamp = salesOrder.ValidUptoDatestamp;
                //dbSalesOrder.CustomerID = salesOrder.CustomerID;
                dbSalesOrder.CustomerPONumber = salesOrder.CustomerPONumber;
                dbSalesOrder.CustomerPODate = salesOrder.CustomerPODate;
                dbSalesOrder.RevisionDatestamp = salesOrder.RevisionDatestamp;
                dbSalesOrder.RevisionNumber = salesOrder.RevisionNumber;
                dbSalesOrder.CompanyBranchLocID = salesOrder.CompanyBranchLocID;
                dbSalesOrder.IsMadeToOrder = salesOrder.IsMadeToOrder;
                dbSalesOrder.AgainstFormID = salesOrder.AgainstFormID;
                dbSalesOrder.SalesLedgerID = salesOrder.SalesLedgerID;
                dbSalesOrder.BillingAddressCode = salesOrder.BillingAddressCode;
                dbSalesOrder.DeliveryAddressCode = salesOrder.DeliveryAddressCode;
                dbSalesOrder.IsPartialShipment = salesOrder.IsPartialShipment;
                dbSalesOrder.ShipmentLocation = salesOrder.ShipmentLocation;
                dbSalesOrder.ShipmentAgentID = salesOrder.ShipmentAgentID;
                dbSalesOrder.SalesPersonID = salesOrder.SalesPersonID;
                dbSalesOrder.ModeOfPaymentID = salesOrder.ModeOfPaymentID;
                dbSalesOrder.PaymentTermID = salesOrder.PaymentTermID;
                dbSalesOrder.CurrencyID = salesOrder.CurrencyID;
                dbSalesOrder.CurrencyXRate = salesOrder.CurrencyXRate;
                dbSalesOrder.AdvanceAmount = salesOrder.AdvanceAmount;
                dbSalesOrder.AdvanceRemarks = salesOrder.AdvanceRemarks;
                dbSalesOrder.AdvanceVoucherID = salesOrder.AdvanceVoucherID;
                dbSalesOrder.IsCompanyVehicle = salesOrder.IsCompanyVehicle;
                dbSalesOrder.CompanyVehicleAssetIndividualID = salesOrder.CompanyVehicleAssetIndividualID;
                dbSalesOrder.DriverEmployeeID = salesOrder.DriverEmployeeID;
                dbSalesOrder.TransporterID = salesOrder.TransporterID;
                dbSalesOrder.TransporterName = salesOrder.TransporterName;
                dbSalesOrder.VehicleNumber = salesOrder.VehicleNumber;
                dbSalesOrder.VehicleType = salesOrder.VehicleType;
                dbSalesOrder.DriverName = salesOrder.DriverName;
                dbSalesOrder.DriverMobileNumber = salesOrder.DriverMobileNumber;
                dbSalesOrder.LicenseNumber = salesOrder.LicenseNumber;
                dbSalesOrder.NetWeight = salesOrder.NetWeight;
                dbSalesOrder.GrossWeight = salesOrder.GrossWeight;
                dbSalesOrder.UnitID = salesOrder.UnitID;
                dbSalesOrder.TotalNumberOfPackings = salesOrder.TotalNumberOfPackings;
                dbSalesOrder.DeclarationID = salesOrder.DeclarationID;
                dbSalesOrder.ResponsibleEmployeeID = salesOrder.ResponsibleEmployeeID;
                dbSalesOrder.StoreInchargeEmployeeID = salesOrder.StoreInchargeEmployeeID;
                dbSalesOrder.DispatchedByEmployeeID = salesOrder.DispatchedByEmployeeID;
                dbSalesOrder.CheckByEmployeeID = salesOrder.CheckByEmployeeID;
                dbSalesOrder.LastModifDatestamp = salesOrder.LastModifDatestamp;
                dbSalesOrder.CustomerRemarks = salesOrder.CustomerRemarks;
                dbSalesOrder.Remarks = salesOrder.Remarks;
                dbSalesOrder.TermsConditions = salesOrder.TermsConditions;
                dbSalesOrder.OtherConditions = salesOrder.OtherConditions;
                if (salesOrder.SalesOrderDetails.Count > 0)
                {
                    dbSalesOrder.SalesOrderDetails = new List<SalesOrderDetail>();
                    foreach (SalesOrderDetailDTOs orderDetail in salesOrder.SalesOrderDetails)
                    {
                        dbSalesOrder.SalesOrderDetails.Add(new SalesOrderDetail
                        {
                            ID = orderDetail.ID,
                            SalesOrderID = orderDetail.SalesOrderID,
                            LineNumber = orderDetail.LineNumber,
                            StockItemID = orderDetail.StockItemID,
                            SubstitutionStockItemID = orderDetail.SubstitutionStockItemID,
                            OrderQty = orderDetail.OrderQty,
                            PlannedQty = orderDetail.PlannedQty,
                            WorkOrderLocID = orderDetail.WorkOrderLocID,
                            DepartmentID = orderDetail.DepartmentID,
                            SalesPersonCommissionAmount = orderDetail.SalesPersonCommissionAmount,
                            DeliveryDate = orderDetail.DeliveryDate,
                            PlannedDeliveryDate = orderDetail.PlannedDeliveryDate,
                            PlannedShipmentDate = orderDetail.PlannedShipmentDate,
                            PlannedInspectionDate = orderDetail.PlannedInspectionDate,
                            ModeOfTransportID = orderDetail.ModeOfTransportID,
                            PriorityID = orderDetail.PriorityID,
                            InspectionDetails = orderDetail.InspectionDetails,
                            MasterPackagingName = orderDetail.MasterPackagingName,
                            MasterPackagingQty = orderDetail.MasterPackagingQty,
                            IsFinalInspectionComplete = orderDetail.IsFinalInspectionComplete,
                            QtyToInvoice = orderDetail.QtyToInvoice,
                            QtyToShip = orderDetail.QtyToShip,
                            Qty = orderDetail.Qty,
                            Rate = orderDetail.Rate,
                            Amount = orderDetail.Amount,
                            DiscountedRate = orderDetail.DiscountedRate,
                            LineAmount = orderDetail.LineAmount,
                            InvoiceDiscountLedgerID = orderDetail.InvoiceDiscountLedgerID,
                            InvoiceDiscount = orderDetail.InvoiceDiscount,
                            ExciseOrServiceTaxLedgerID = orderDetail.ExciseOrServiceTaxLedgerID,
                            ExciseOrServiceTax = orderDetail.ExciseOrServiceTax,
                            ServiceTaxEducationCessLedgerID = orderDetail.ServiceTaxEducationCessLedgerID,
                            ServiceTaxEducationCess = orderDetail.ServiceTaxEducationCess,
                            ServiceTaxSHSEducationCessLedgerID = orderDetail.ServiceTaxSHSEducationCessLedgerID,
                            ServiceTaxSHSEducationCess = orderDetail.ServiceTaxSHSEducationCess,
                            FreightLocalLedgerID = orderDetail.FreightLocalLedgerID,
                            FreightLocal = orderDetail.FreightLocal,
                            FreightEximLedgerID = orderDetail.FreightEximLedgerID,
                            FreightExim = orderDetail.FreightExim,
                            TransitInsuranceLedgerID = orderDetail.TransitInsuranceLedgerID,
                            TransitInsurance = orderDetail.TransitInsurance,
                            PackingChargesLedgerID = orderDetail.PackingChargesLedgerID,
                            PackingCharges = orderDetail.PackingCharges,
                            LoadingUnloadingLedgerID = orderDetail.LoadingUnloadingLedgerID,
                            LoadingUnloading = orderDetail.LoadingUnloading,
                            MiscExpLedgerID = orderDetail.MiscExpLedgerID,
                            MiscExp = orderDetail.MiscExp,
                            SalesTaxLedgerID = orderDetail.SalesTaxLedgerID,
                            SalesTax = orderDetail.SalesTax,
                            CentralSalesTaxLedgerID = orderDetail.CentralSalesTaxLedgerID,
                            CentralSalesTax = orderDetail.CentralSalesTax,
                            TotalBillAmt = orderDetail.TotalBillAmt,
                            FreightLocalNonRecvLedgerID = orderDetail.FreightLocalNonRecvLedgerID,
                            FreightLocalNonRecv = orderDetail.FreightLocalNonRecv,
                            FreightEximNonRecvLedgerID = orderDetail.FreightEximNonRecvLedgerID,
                            FreightEximNonRecv = orderDetail.FreightEximNonRecv,
                            TransitInsuranceNonRecvLedgerID = orderDetail.TransitInsuranceNonRecvLedgerID,
                            TransitInsuranceNonRecv = orderDetail.TransitInsuranceNonRecv,
                            PackingChargesNonRecvLedgerID = orderDetail.PackingChargesNonRecvLedgerID,
                            PackingChargesNonRecv = orderDetail.PackingChargesNonRecv,
                            LoadingUnloadingNonRecvLedgerID = orderDetail.LoadingUnloadingNonRecvLedgerID,
                            LoadingUnloadingNonRecv = orderDetail.LoadingUnloadingNonRecv,
                            MiscExpNonRecvLedgerID = orderDetail.MiscExpNonRecvLedgerID,
                            MiscExpNonRecv = orderDetail.MiscExpNonRecv,
                            SalesCommissionNonRecvLedgerID = orderDetail.SalesCommissionNonRecvLedgerID,
                            SalesCommissionNonRecv = orderDetail.SalesCommissionNonRecv,
                            CustomDutyBasicNonRecv = orderDetail.CustomDutyBasicNonRecv,
                            CustomDutyBasicNonRecvLedgerID = orderDetail.CustomDutyBasicNonRecvLedgerID,
                            AdditionalCustomDutyNonRecvLedgerID = orderDetail.AdditionalCustomDutyNonRecvLedgerID,
                            AdditionalCustomDutyNonRecv = orderDetail.AdditionalCustomDutyNonRecv,
                            SpecialAdditionalCustomDutyNonRecvLedgerID = orderDetail.SpecialAdditionalCustomDutyNonRecvLedgerID,
                            SpecialAdditionalCustomDutyNonRecv = orderDetail.SpecialAdditionalCustomDutyNonRecv,
                            AntiDumpingDutyNonRecvLedgerID = orderDetail.AntiDumpingDutyNonRecvLedgerID,
                            AntiDumpingDutyNonRecv = orderDetail.AntiDumpingDutyNonRecv,
                            ImportVATNonRecvLedgerID = orderDetail.ImportVATNonRecvLedgerID,
                            ImportVATNonRecv = orderDetail.ImportVATNonRecv,
                            ImportExiseOrServiceTaxNonRecv = orderDetail.ImportExiseOrServiceTaxNonRecv,
                            ImportExiseOrServiceTaxNonRecvLedgerID = orderDetail.ImportExiseOrServiceTaxNonRecvLedgerID,
                            AgentChargesNonRecvLedgerID = orderDetail.AgentChargesNonRecvLedgerID,
                            AgentChargesNonRecv = orderDetail.AgentChargesNonRecv,
                            LocID = orderDetail.LocID,
                            IsExciseonQty = orderDetail.IsExciseonQty,
                            OrderStatusID = orderDetail.OrderStatusID,
                            Remarks = orderDetail.Remarks
                        });
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dbSalesOrder;
        }
    }
}