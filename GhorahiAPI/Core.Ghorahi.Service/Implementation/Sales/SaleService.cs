﻿using Core.Ghorahi.Service.Models.Sales.SpResult;
using Core.Ghorahi.Service.Models.Sales.Request;
using Core.Ghorahi.Service.Models.Messages;
using Core.Ghorahi.Service.Models.Sales;
using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Service.Converter;
using Core.Ghorahi.Data.Interface;
using System.Collections.Generic;
using Core.Ghorahi.Data.Entity;
using System.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using Newtonsoft.Json;
using Core.Ghorahi.Service.Models.FirebasePush;
using System.Text;
using Core.Ghorahi.Common.Manager;

namespace Core.Ghorahi.Service.Implementation.Sales
{
    public class SaleService : ISaleService
    {
        private IRepository repository;
        public SaleService(IRepository repository)
        {
            this.repository = repository;
        }

        public ResponseMessage<List<SalesDispatchRegister>> GetSalesDispatchRegister(SalesDispatchRegisterRequest dispatchRequest)
        {
            ResponseMessage<List<SalesDispatchRegister>> responseMessage = new ResponseMessage<List<SalesDispatchRegister>>
            {
                ResponseValue = new List<SalesDispatchRegister>()
            };
            try
            {
                if (Scheduler.Sale.IsValidRequest(dispatchRequest))
                {
                    long customerId = -999;
                    string customerIds = "";
                    if (dispatchRequest.IsAll)
                    {
                        customerIds = Agents.Scheduler.Agent.ConcateCustomerIds((long)dispatchRequest.AgentId);
                    }
                    else
                    {
                        Data.Entity.Inventory.Party dbParty = repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals((long)dispatchRequest.CustomerId));
                        if (dbParty != null)
                        {
                            customerId = long.Parse(dbParty.Remarks.Split('|')[1]);
                        }
                    }
                    IQueryable<usp_x_GetSalesDispatchRegister_Result> dbSalesDispatchRegisters = repository.Context.usp_x_GetSalesDispatchRegister(dispatchRequest.FromDate, dispatchRequest.ToDate, customerId, -999, -999, -999, -999, -999, customerIds, -999, -999).Take(100).AsQueryable();
                    foreach (var dbSalesDispatchRegister in dbSalesDispatchRegisters)
                    {
                        responseMessage.ResponseValue.Add(SalesConverter.ConvertToModel(dbSalesDispatchRegister, new SalesDispatchRegister()));
                    }
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<List<SalesInvoiceRegister>> GetSalesInvoiceRegister(SalesInvoiceRegisterRequest invoiceRequest)
        {
            ResponseMessage<List<SalesInvoiceRegister>> responseMessage = new ResponseMessage<List<SalesInvoiceRegister>>
            {
                ResponseValue = new List<SalesInvoiceRegister>()
            };
            try
            {
                if (Scheduler.Sale.IsValidRequest(invoiceRequest))
                {
                    long customerId = -999;
                    string customerIds = "";
                    if (invoiceRequest.IsAll)
                    {
                        customerIds = Agents.Scheduler.Agent.ConcateCustomerIds((long)invoiceRequest.AgentId);
                    }
                    else
                    {
                        Data.Entity.Inventory.Party dbParty = repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals((long)invoiceRequest.CustomerId));
                        if (dbParty != null)
                        {
                            customerId = long.Parse(dbParty.Remarks.Split('|')[1]);
                        }
                    }
                    IQueryable<usp_x_GetSalesInvoiceRegister_Result> dbSalesInvoiceRegisters = repository.Context.usp_x_GetSalesInvoiceRegister(invoiceRequest.FromDate, invoiceRequest.ToDate, customerId, -999, -999, -999, -999, -999, -999, -999, -999, customerIds, -999).Take(100).AsQueryable();
                    foreach (var dbSalesInvoiceRegister in dbSalesInvoiceRegisters)
                    {
                        responseMessage.ResponseValue.Add(SalesConverter.ConvertToModel(dbSalesInvoiceRegister, new SalesInvoiceRegister()));
                    }
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<List<SalesOrderRegister>> GetSalesOrdersRegister(SalesOrderRegisterRequest orderRequest)
        {
            ResponseMessage<List<SalesOrderRegister>> responseMessage = new ResponseMessage<List<SalesOrderRegister>>
            {
                ResponseValue = new List<SalesOrderRegister>()
            };
            try
            {
                if (Scheduler.Sale.IsValidRequest(orderRequest))
                {
                    long customerId = -999;
                    string customerIds = "";
                    if (orderRequest.IsAll)
                    {
                        customerIds = Agents.Scheduler.Agent.ConcateCustomerIds((long)orderRequest.AgentId);
                    }
                    else
                    {
                        Data.Entity.Inventory.Party dbParty = repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals((long)orderRequest.CustomerId));
                        if (dbParty != null)
                        {
                            customerId = long.Parse(dbParty.Remarks.Split('|')[1]);
                        }
                    }
                    IQueryable<usp_x_GetSalesOrderRegister_Result> dbSalesOrderRegisters = repository.Context.usp_x_GetSalesOrderRegister(orderRequest.FromDate, orderRequest.ToDate, customerId, -999, -999, -999, customerIds).Take(100).AsQueryable();
                    if (dbSalesOrderRegisters != null)
                    {
                        foreach (usp_x_GetSalesOrderRegister_Result dbSalesOrderRegister in dbSalesOrderRegisters)
                        {
                            responseMessage.ResponseValue.Add(SalesConverter.ConvertToModel(dbSalesOrderRegister, new SalesOrderRegister()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<List<SalesLedgerRegister>> GetSalesLedgerRegister(SalesLedgerRequest salesLedgerRequest)
        {
            ResponseMessage<List<SalesLedgerRegister>> responseMessage = new ResponseMessage<List<SalesLedgerRegister>>
            {
                ResponseValue = new List<SalesLedgerRegister>()
            };
            try
            {
                Data.Entity.Inventory.Party dbParty = repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals((long)salesLedgerRequest.CustomerId));
                if (dbParty != null)
                {
                    string[] arrayCode = dbParty.Remarks.Split('|');
                    long customerId = long.Parse(arrayCode[1]);
                    string branchCode = arrayCode[2];
                    CustomerBranch dbCustomerBranch = repository.Context.CustomerBranches.FirstOrDefault(fd => fd.CustomerID.Equals(customerId) && fd.Code.Equals(branchCode));
                    if (dbCustomerBranch != null)
                    {
                        long ledgerId = dbCustomerBranch.LedgerID;
                        IQueryable<usp_SingleLineLedgerViewReport_Result> dbSalesLedgers = repository.Context.usp_SingleLineLedgerViewReport(ledgerId, salesLedgerRequest.FromDate, salesLedgerRequest.ToDate, 0, false).AsQueryable();
                        foreach (usp_SingleLineLedgerViewReport_Result dbSalesLedger in dbSalesLedgers)
                        {
                            responseMessage.ResponseValue.Add(SalesConverter.ConvertToModel(dbSalesLedger, new SalesLedgerRegister()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<int> SaveSalesOrder(SalesOrderRequest salesOrderRequest)
        {
            ResponseMessage<int> responseMessage = new ResponseMessage<int>();
            try
            {
                SalesOrderDTOs salesOrder = new SalesOrderDTOs { SalesOrderDetails = new List<SalesOrderDetailDTOs>() };
                salesOrder.OrderDate = salesOrderRequest.OrderDate;
                salesOrder.CustomerID = salesOrderRequest.CustomerId;
                salesOrder.ShipmentAgentID = salesOrderRequest.AgentId;
                salesOrder.LastModifDatestamp = salesOrder.OrderDate;
                int lineNumber = 0;
                foreach (SalesOrderDetials orderDetail in salesOrderRequest.SalesOrderDetails)
                {
                    lineNumber++;
                    SalesOrderDetailDTOs salesOrderDetail = new SalesOrderDetailDTOs
                    {
                        StockItemID = orderDetail.StockItemId,
                        Qty = orderDetail.Qty,
                        Rate = orderDetail.Rate,
                        LineNumber = lineNumber,
                        IsExciseonQty = orderDetail.ServiceTax > 0 ? true : false,
                        ExciseOrServiceTax = orderDetail.ServiceTax * orderDetail.Qty,
                        DiscountedRate = orderDetail.Rate// - orderDetail.Discount
                    };
                    salesOrderDetail.LineAmount = orderDetail.Qty * salesOrderDetail.DiscountedRate;
                    salesOrderDetail.InvoiceDiscount = salesOrderDetail.Qty * orderDetail.Discount;
                    salesOrderDetail.SalesTax = (salesOrderDetail.LineAmount + salesOrderDetail.ExciseOrServiceTax - salesOrderDetail.InvoiceDiscount) * (orderDetail.Vat / 100);
                    salesOrderDetail.TotalBillAmt = salesOrderDetail.LineAmount + salesOrderDetail.ExciseOrServiceTax + salesOrderDetail.SalesTax;
                    salesOrderDetail.OrderStatusID = orderDetail.OrderStatusId;
                    salesOrder.SalesOrderDetails.Add(salesOrderDetail);
                }
                C_autotext dbAutoText = repository.Context.C_autotext.Where(w => w.TableName.Equals("SalesOrder", StringComparison.InvariantCultureIgnoreCase)).OrderByDescending(o => o.EffFromDate).FirstOrDefault();
                string customerCode = repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals(salesOrder.CustomerID)).Remarks;
                string[] arrayCode = customerCode.Split('|');
                long customerId = long.Parse(arrayCode[1]);
                string branchCode = arrayCode[2];
                CustomerBranch customerBranch = repository.Context.CustomerBranches.FirstOrDefault(fd => fd.CustomerID.Equals(customerId) && fd.Code.Equals(branchCode));
                if (customerBranch != null)
                {
                    salesOrder.BillingAddressCode = customerBranch.Code;
                    salesOrder.DeliveryAddressCode = customerBranch.Code;
                    salesOrder.ShipmentLocation = customerBranch.City.Name;
                }
                SalesOrder dbSalesOrder = SalesConverter.ConvertToEntity(salesOrder, new SalesOrder());
                dbSalesOrder.CustomerID = customerId;
                string dataSeed = "";
                for (int i = ((int)dbAutoText.Seed).ToString().Length; i < 5; i++)
                {
                    dataSeed += "0";
                }
                dataSeed += $"{dbAutoText.Seed}";
                dbSalesOrder.Code = dbAutoText.Pattern.Replace("<<@>>", dataSeed);
                repository.Insert(dbSalesOrder);
                dbAutoText.Seed++;
                responseMessage.ResponseValue = repository.SaveChanges();
                if (responseMessage.ResponseValue > 0)// && SessionManager.IsCustomer)
                {
                    var url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?";
                    var jsonoobjdata = _download_serialized_json_data<GoolgeSignInResponse>(url);
                    if (jsonoobjdata != null)
                    {
                        var url1 = "https://ghorahipushapi.firebaseio.com/userdata/" + salesOrderRequest.AgentId + ".json?auth=" + jsonoobjdata.idToken;
                        var jsonoobjdata1 = _download_serialized_json_data1<FireBaseResponse>(url1);
                        if (jsonoobjdata1 != null)
                        {
                            Customer dbCustomer = repository.Context.Customers.FirstOrDefault(fd => fd.ID.Equals(dbSalesOrder.CustomerID));
                            if (dbCustomer != null)
                            {
                                var sendreponse = SendPushNotification(jsonoobjdata1.UUID, dbCustomer.Name);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
            }
            return responseMessage;
        }

        //POST
        private static T _download_serialized_json_data<T>(string url) where T : new()
        {
            using (var wc = new WebClient())
            {
                var json_data = string.Empty;
                var values = new System.Collections.Specialized.NameValueCollection();
                values.Add("key", "AIzaSyASaNajsrxNYMRf-puDkyNX5Qq6IkQpGNQ");
                try
                {
                    var responsebytes = wc.UploadValues(url, "POST", values);
                    var responsebody = new UTF8Encoding().GetString(responsebytes);
                    json_data = responsebody;
                }
                catch (Exception ex) { throw ex; }
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }

        //GET
        private static T _download_serialized_json_data1<T>(string url) where T : new()
        {
            using (var wc = new WebClient())
            {
                var json_data = string.Empty;
                try
                {
                    string getpage = wc.DownloadString(url);
                    //MessageBox.Show(getpage);
                    json_data = getpage;
                }
                catch (Exception ex) { throw ex; }
                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json_data) : new T();
            }
        }

        public static dynamic SendPushNotification(string ExpoToken, string customerName)
        {
            dynamic body = new
            {
                to = ExpoToken,
                title = "Sales Order",
                body = $"New Sales Order Created by {customerName}",
                sound = "default",
                //data = new { some = "daaaata" }
            };
            string response = null;
            using (WebClient client = new WebClient())
            {
                try
                {
                    client.Headers.Add("accept", "application/json");
                    client.Headers.Add("accept-encoding", "gzip, deflate");
                    client.Headers.Add("Content-Type", "application/json");
                    response = client.UploadString("https://exp.host/--/api/v2/push/send", JsonConvert.SerializeObject(body));
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            var json = JsonConvert.SerializeObject(response);
            return json;
        }

        public ResponseMessage<List<SalesOrderHold>> GetHoldSalesOrder(HoldSalesOrder holdSalesOrder)
        {
            ResponseMessage<List<SalesOrderHold>> responseMessage = new ResponseMessage<List<SalesOrderHold>>
            {
                ResponseValue = new List<SalesOrderHold>()
            };
            try
            {
                long customerId = -999;
                IQueryable<SalesOrder> dbSalesOrders = null;
                if (holdSalesOrder.IsAll)
                {
                    List<long> customers = Agents.Scheduler.Agent.GetCustomers(holdSalesOrder.AgentId);
                    dbSalesOrders = repository.Context.SalesOrders.Where(w => customers.Contains(w.CustomerID) && w.ShipmentAgentID.Equals(holdSalesOrder.AgentId) && w.SalesOrderDetails.Any(a => a.OrderStatusID.Equals(holdSalesOrder.OrderStatusId)));
                }
                else
                {
                    Data.Entity.Inventory.Party dbParty = repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals(holdSalesOrder.CustomerId));
                    if (dbParty != null)
                    {
                        customerId = long.Parse(dbParty.Remarks.Split('|')[1]);
                    }
                    dbSalesOrders = repository.Context.SalesOrders.Where(w => w.CustomerID.Equals(customerId) && w.ShipmentAgentID.Equals(holdSalesOrder.AgentId) && w.SalesOrderDetails.Any(a => a.OrderStatusID.Equals(holdSalesOrder.OrderStatusId)));
                }
                foreach (SalesOrder dbSalesOrder in dbSalesOrders)
                {
                    responseMessage.ResponseValue.Add(SalesConverter.ConvertToModel(dbSalesOrder, new SalesOrderHold()));
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<int> UpdateOrderStatus(UpdateOrderStatusRequest updateOrderStatus)
        {
            ResponseMessage<int> responseMessage = new ResponseMessage<int>();
            try
            {
                Data.Entity.Inventory.Party dbParty = repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals(updateOrderStatus.CustomerId));
                if (dbParty != null)
                {
                    long customerId = long.Parse(dbParty.Remarks.Split('|')[1]);
                    SalesOrderDetail dbSalesOrderDetail = repository.Context.SalesOrderDetails.FirstOrDefault(fd => fd.SalesOrder.CustomerID.Equals(customerId) && fd.SalesOrder.ShipmentAgentID.Equals(updateOrderStatus.AgentId) && fd.SalesOrderID.Equals(updateOrderStatus.SalesOrderId) && fd.ID.Equals(updateOrderStatus.SalesOrderDetailsId));
                    if (dbSalesOrderDetail != null)
                    {
                        dbSalesOrderDetail.OrderStatusID = updateOrderStatus.OrderStatusId;
                        responseMessage.ResponseValue = repository.Context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }
    }
}