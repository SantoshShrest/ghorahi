﻿using Core.Ghorahi.Service.Models.Sales;
using Core.Ghorahi.Data.Interface;
using Core.Ghorahi.Data;
using System;

namespace Core.Ghorahi.Service.Implementation.Sales.Scheduler
{
    public sealed class Sale
    {
        private static IRepository Repository { get { return new Repository(); } }
        public static bool IsValidRequest(SalesDispatchRegisterRequest dispatchRequest)
        {
            bool isValid = false;
            try
            {
                isValid = !string.IsNullOrWhiteSpace(dispatchRequest.FromDate);
                isValid = !string.IsNullOrWhiteSpace(dispatchRequest.ToDate);
                isValid = dispatchRequest.AgentId > 0;
                isValid = !dispatchRequest.IsAll ? dispatchRequest.CustomerId > 0 : true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isValid;
        }

        public static bool IsValidRequest(SalesOrderRegisterRequest orderRegisterRequest)
        {
            bool isValid = false;
            try
            {
                isValid = !string.IsNullOrWhiteSpace(orderRegisterRequest.FromDate);
                isValid = !string.IsNullOrWhiteSpace(orderRegisterRequest.ToDate);
                isValid = orderRegisterRequest.AgentId > 0;
                isValid = !orderRegisterRequest.IsAll ? orderRegisterRequest.CustomerId > 0 : true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isValid;
        }

        public static bool IsValidRequest(SalesInvoiceRegisterRequest invoiceRegisterRequest)
        {
            bool isValid = false;
            try
            {
                isValid = !string.IsNullOrWhiteSpace(invoiceRegisterRequest.FromDate);
                isValid = !string.IsNullOrWhiteSpace(invoiceRegisterRequest.ToDate);
                isValid = invoiceRegisterRequest.AgentId > 0;
                isValid = !invoiceRegisterRequest.IsAll ? invoiceRegisterRequest.CustomerId > 0 : true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return isValid;
        }
    }
}
