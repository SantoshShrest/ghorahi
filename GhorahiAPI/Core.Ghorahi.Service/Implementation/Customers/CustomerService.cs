﻿using Core.Ghorahi.Common.Enumeration;
using Core.Ghorahi.Common.Manager;
using Core.Ghorahi.Data.Entity;
using Core.Ghorahi.Data.Interface;
using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Service.Models.Customers;
using Core.Ghorahi.Service.Models.Customers.Response;
using Core.Ghorahi.Service.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Service.Implementation.Customers
{
    public class CustomerService : ICustomerService
    {
        private IRepository repository;
        public CustomerService(IRepository repository)
        {
            this.repository = repository;
        }

        public ResponseMessage<List<CustomerNotificationResponse>> GetDispatchOrderNotice(long customerId)
        {
            ResponseMessage<List<CustomerNotificationResponse>> responseMessage = new ResponseMessage<List<CustomerNotificationResponse>>
            {
                ResponseValue = new List<CustomerNotificationResponse>()
            };
            try
            {
                responseMessage.ResponseValue = DataManager.CustomerDispatchNotifications.Where(w => w.CustomerId.Equals(customerId) && !w.IsNotified)
                                                                                          .Select(s => new CustomerNotificationResponse
                                                                                          {
                                                                                              AgentId = s.AgentId,
                                                                                              CustomerId = s.CustomerId,
                                                                                              IsNotified = s.IsNotified,
                                                                                              ItemName = s.ItemName,
                                                                                              ItemQuantity = s.ItemQuantity,
                                                                                              OrderDate = s.OrderDate,
                                                                                              OrderStatus = s.OrderStatus,
                                                                                              OrderId = s.OrderId,
                                                                                              LastModifiedDate = s.LastModifiedDate,
                                                                                              ResponseType = s.ResponseType
                                                                                          }).ToList();
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                //throw;
            }
            return responseMessage;
        }

        public ResponseMessage<List<CustomerNotificationResponse>> GetRegisterOrderNotice(long customerId)
        {
            ResponseMessage<List<CustomerNotificationResponse>> responseMessage = new ResponseMessage<List<CustomerNotificationResponse>>
            {
                ResponseValue = new List<CustomerNotificationResponse>()
            };
            try
            {
                responseMessage.ResponseValue = DataManager.CustomerSalesNotifications.Where(w => w.CustomerId.Equals(customerId) && !w.IsNotified)
                    .Select(s => new CustomerNotificationResponse
                    {
                        AgentId = s.AgentId,
                        CustomerId = s.CustomerId,
                        IsNotified = s.IsNotified,
                        ItemName = s.ItemName,
                        ItemQuantity = s.ItemQuantity,
                        OrderDate = s.OrderDate,
                        OrderStatus = s.OrderStatus,
                        OrderId = s.OrderId,
                        LastModifiedDate = s.LastModifiedDate,
                        ResponseType = s.ResponseType
                    }).ToList();
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                //throw;
            }
            return responseMessage;
        }

        public ResponseMessage<SahyogiSchemeResponse> GetSahyogiScheme(long customerId)
        {
            ResponseMessage<SahyogiSchemeResponse> responseMessage = new ResponseMessage<SahyogiSchemeResponse>
            {
                ResponseValue = new SahyogiSchemeResponse()
            };
            try
            {
                Data.Entity.Inventory.Party dbParty = repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals(customerId));
                if (dbParty != null)
                {
                    int id = int.Parse(dbParty.Remarks.Split('|')[1]);
                    SahyogiCustomerInfo dbSahyogiCustomerInfo = repository.Context.SahyogiCustomerInfoes.FirstOrDefault(fd => fd.CustomerId.Equals(id));
                    if (dbSahyogiCustomerInfo != null)
                    {
                        SahyogiCustomerPointsInfo dbSahyogiPointsInfo = dbSahyogiCustomerInfo.SahyogiCustomerPointsInfoes.FirstOrDefault();
                        SahyogiProduct dbSahyogiProduct = repository.Context.SahyogiProducts.FirstOrDefault(fd => fd.ShProductId.Equals(dbSahyogiPointsInfo.ShProductId));
                        SahyogiSchemeDetail dbSahyogiSchemeDetail = repository.Context.SahyogiSchemeDetails.FirstOrDefault(fd => fd.SchemeDetail.Equals("Points required", StringComparison.CurrentCultureIgnoreCase));
                        responseMessage.ResponseValue.CurrentCategory = dbSahyogiPointsInfo.PointsQty >= int.Parse(dbSahyogiSchemeDetail.Sagarmatha) ? (int)SchemeCategory.Sagarmatha : dbSahyogiPointsInfo.PointsQty >= int.Parse(dbSahyogiSchemeDetail.K2) ? (int)SchemeCategory.K2 : dbSahyogiPointsInfo.PointsQty >= int.Parse(dbSahyogiSchemeDetail.Kanchanjunga) ? (int)SchemeCategory.Kanchanjunga : dbSahyogiPointsInfo.PointsQty >= int.Parse(dbSahyogiSchemeDetail.Lhotse) ? (int)SchemeCategory.Lhotse : dbSahyogiPointsInfo.PointsQty >= int.Parse(dbSahyogiSchemeDetail.Makalu) ? (int)SchemeCategory.Makhalu : (int)SchemeCategory.NoCategory;
                        responseMessage.ResponseValue.NextCategory = responseMessage.ResponseValue.CurrentCategory.Equals((int)SchemeCategory.Sagarmatha) ? 0 : responseMessage.ResponseValue.CurrentCategory + 1;
                        responseMessage.ResponseValue.PointsEarned = dbSahyogiPointsInfo.PointsQty;
                        switch ((SchemeCategory)responseMessage.ResponseValue.NextCategory)
                        {
                            case SchemeCategory.NoCategory:
                            case SchemeCategory.Makhalu:
                                responseMessage.ResponseValue.PointsRequired = int.Parse(dbSahyogiSchemeDetail.Makalu) - dbSahyogiPointsInfo.PointsQty;
                                break;
                            case SchemeCategory.Lhotse:
                                responseMessage.ResponseValue.PointsRequired = int.Parse(dbSahyogiSchemeDetail.Lhotse) - dbSahyogiPointsInfo.PointsQty;
                                break;
                            case SchemeCategory.Kanchanjunga:
                                responseMessage.ResponseValue.PointsRequired = int.Parse(dbSahyogiSchemeDetail.Kanchanjunga) - dbSahyogiPointsInfo.PointsQty;
                                break;
                            case SchemeCategory.K2:
                                responseMessage.ResponseValue.PointsRequired = int.Parse(dbSahyogiSchemeDetail.K2) - dbSahyogiPointsInfo.PointsQty;
                                break;
                            case SchemeCategory.Sagarmatha:
                                responseMessage.ResponseValue.PointsRequired = int.Parse(dbSahyogiSchemeDetail.Sagarmatha) - dbSahyogiPointsInfo.PointsQty;
                                break;
                            default:
                                responseMessage.ResponseValue.PointsRequired = int.Parse(dbSahyogiSchemeDetail.Makalu) - dbSahyogiPointsInfo.PointsQty;
                                break;
                        }
                        int[] facilityIds = { 1, 2 };
                        IQueryable<SahyogiSchemeFacility> dbSchemeBagFacilities = repository.Context.SahyogiSchemeFacilities.Where(fd => facilityIds.Contains(fd.FacilityId));
                        switch ((SchemeCategory)responseMessage.ResponseValue.CurrentCategory)
                        {
                            case SchemeCategory.NoCategory:
                                responseMessage.ResponseValue.TotalDiscountedBags = 0;
                                responseMessage.ResponseValue.DiscountPerBag = "Rs 0";
                                break;
                            case SchemeCategory.Makhalu:
                                responseMessage.ResponseValue.TotalDiscountedBags = int.Parse(dbSchemeBagFacilities.FirstOrDefault(fd => fd.FacilityId.Equals(2)).Makalu.Split(' ')[0]);
                                responseMessage.ResponseValue.DiscountPerBag = dbSchemeBagFacilities.FirstOrDefault(Fd => Fd.FacilityId.Equals(1)).Makalu;
                                break;
                            case SchemeCategory.Lhotse:
                                responseMessage.ResponseValue.TotalDiscountedBags = int.Parse(dbSchemeBagFacilities.FirstOrDefault(fd => fd.FacilityId.Equals(2)).Lhotse.Split(' ')[0]);
                                responseMessage.ResponseValue.DiscountPerBag = dbSchemeBagFacilities.FirstOrDefault(Fd => Fd.FacilityId.Equals(1)).Lhotse;
                                break;
                            case SchemeCategory.Kanchanjunga:
                                responseMessage.ResponseValue.TotalDiscountedBags = int.Parse(dbSchemeBagFacilities.FirstOrDefault(fd => fd.FacilityId.Equals(2)).Kanchanjunga.Split(' ')[0]);
                                responseMessage.ResponseValue.DiscountPerBag= dbSchemeBagFacilities.FirstOrDefault(Fd => Fd.FacilityId.Equals(1)).Kanchanjunga;
                                break;
                            case SchemeCategory.K2:
                                responseMessage.ResponseValue.TotalDiscountedBags = int.Parse(dbSchemeBagFacilities.FirstOrDefault(fd => fd.FacilityId.Equals(2)).K2.Split(' ')[0]);
                                responseMessage.ResponseValue.DiscountPerBag = dbSchemeBagFacilities.FirstOrDefault(Fd => Fd.FacilityId.Equals(1)).K2;
                                break;
                            case SchemeCategory.Sagarmatha:
                                responseMessage.ResponseValue.TotalDiscountedBags = int.Parse(dbSchemeBagFacilities.FirstOrDefault(fd => fd.FacilityId.Equals(2)).Sagarmatha.Split(' ')[0]);
                                responseMessage.ResponseValue.DiscountPerBag = dbSchemeBagFacilities.FirstOrDefault(Fd => Fd.FacilityId.Equals(1)).Sagarmatha;
                                break;
                            default:
                                responseMessage.ResponseValue.TotalDiscountedBags = 0;
                                responseMessage.ResponseValue.DiscountPerBag = "Rs 0";
                                break;
                        }
                        responseMessage.ResponseValue.UsedDiscountedBags = responseMessage.ResponseValue.TotalDiscountedBags - dbSahyogiPointsInfo.PointsRemainQty;//PointsRemainingQty is No. of Remaining Bags
                        responseMessage.ResponseValue.RemainingDiscountedBags = dbSahyogiPointsInfo.PointsRemainQty;
                    }
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                //throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<List<SahabhagiSchemeDetailDTOs>> GetSahabhagiSchemeDetails(long customerId)
        {
            ResponseMessage<List<SahabhagiSchemeDetailDTOs>> responseMessage = new ResponseMessage<List<SahabhagiSchemeDetailDTOs>>
            {
                ResponseValue = new List<SahabhagiSchemeDetailDTOs>()
            };
            try
            {
                IQueryable<SahyogiSchemeDetail> dbSahyogiSchemeDetails = repository.Context.SahyogiSchemeDetails.Where(w => w.Active);
                responseMessage.ResponseValue = new List<SahabhagiSchemeDetailDTOs>();
                foreach (SahyogiSchemeDetail dbSahyogiSchemeDetail in dbSahyogiSchemeDetails)
                {
                    responseMessage.ResponseValue.Add(new SahabhagiSchemeDetailDTOs
                    {
                        Active = dbSahyogiSchemeDetail.Active,
                        CreatedBy = dbSahyogiSchemeDetail.CreatedBy,
                        CreationDatestamp = dbSahyogiSchemeDetail.CreationDatestamp,
                        DetailId = dbSahyogiSchemeDetail.DetailId,
                        K2 = dbSahyogiSchemeDetail.K2,
                        Kanchanjunga = dbSahyogiSchemeDetail.Kanchanjunga,
                        Lhotse = dbSahyogiSchemeDetail.Lhotse,
                        Makalu = dbSahyogiSchemeDetail.Makalu,
                        Sagarmatha = dbSahyogiSchemeDetail.Sagarmatha,
                        SchemeDetail = dbSahyogiSchemeDetail.SchemeDetail,
                        Unit = dbSahyogiSchemeDetail.Unit
                    });
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                //throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<List<SahabhagiSchemeFacilityDTOs>> GetSahabhagiSchemeFacilities(long customerId)
        {
            ResponseMessage<List<SahabhagiSchemeFacilityDTOs>> responseMessage = new ResponseMessage<List<SahabhagiSchemeFacilityDTOs>>
            {
                ResponseValue = new List<SahabhagiSchemeFacilityDTOs>()
            };
            try
            {
                IQueryable<SahyogiSchemeFacility> dbSahyogiSchemeFacilities = repository.Context.SahyogiSchemeFacilities.Where(w => w.Active);
                responseMessage.ResponseValue = new List<SahabhagiSchemeFacilityDTOs>();
                foreach (SahyogiSchemeFacility dbSahyogiSchemeFacility in dbSahyogiSchemeFacilities)
                {
                    responseMessage.ResponseValue.Add(new SahabhagiSchemeFacilityDTOs
                    {
                        Active = dbSahyogiSchemeFacility.Active,
                        CreatedBy = dbSahyogiSchemeFacility.CreatedBy,
                        CreationDatestamp = dbSahyogiSchemeFacility.CreationDatestamp,
                        Facilities = dbSahyogiSchemeFacility.Facilities,
                        FacilityId = dbSahyogiSchemeFacility.FacilityId,
                        K2 = dbSahyogiSchemeFacility.K2,
                        Kanchanjunga = dbSahyogiSchemeFacility.Kanchanjunga,
                        Lhotse = dbSahyogiSchemeFacility.Lhotse,
                        Makalu = dbSahyogiSchemeFacility.Makalu,
                        Sagarmatha = dbSahyogiSchemeFacility.Sagarmatha
                    });
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                //throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<List<SalesInvoiceResult>> GetDiscountedBags(long customerId)
        {
            ResponseMessage<List<SalesInvoiceResult>> responseMessage = new ResponseMessage<List<SalesInvoiceResult>>
            {
                ResponseValue = new List<SalesInvoiceResult>()
            };
            try
            {
                Data.Entity.Inventory.Party dbParty = repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals(customerId));
                if (dbParty != null)
                {
                    int id = int.Parse(dbParty.Remarks.Split('|')[1]);
                    responseMessage.ResponseValue = repository.Context.usp_x_GetSalesInvoiceRegister("", "", id, -999, -999, -999, -999, -999, -999, -999, -999, "", -999)
                                                                      .Where(w => w.Discount > 0).OrderByDescending(d => d.Invoice_Date).Take(10)
                                                                      .Select(s => new SalesInvoiceResult
                                                                      {
                                                                          Amount = decimal.Parse(((decimal)s.Bill_Amt).ToString("F")),
                                                                          Date = s.Invoice_Date,
                                                                          Discount = decimal.Parse(((decimal)s.Discount).ToString("F")),
                                                                          InvoiceNo = s.Invoice_No_,
                                                                          Quantity = decimal.Parse(((decimal)s.Invoice_Qty).ToString("F"))
                                                                      }).ToList();
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                //throw ex;
            }
            return responseMessage;
        }
    }
}