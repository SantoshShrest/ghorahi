﻿using Core.Ghorahi.Data.Interface;
using System.Collections.Generic;
using Core.Ghorahi.Data;
using System.Linq;
using System;

namespace Core.Ghorahi.Service.Implementation.Agents.Scheduler
{
    public sealed class Agent
    {
        private static IRepository Repository { get { return new Repository(); } }
        public static List<long> GetCustomers(long agentId)
        {
            List<long> customerIds = new List<long>();
            try
            {
                string agentCode = $"ID|{agentId}";
                List<string> partyCodes = Repository.InventoryContext.C_AgentPartyMap.Where(w => w.Agent.Equals(agentCode)).Select(s => s.Party).ToList();
                foreach (string partyCode in partyCodes)
                {
                    customerIds.Add(long.Parse(partyCode.Split('|')[1]));
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return customerIds.Distinct().ToList();
        }

        public static long GetCustomerIdFromInventory(long tcdCustomerId)
        {
            long partyId = 0;
            try
            {
                string code = $"|{tcdCustomerId}|";
                Core.Ghorahi.Data.Entity.Inventory.Party dbParty = Repository.InventoryContext.Parties.FirstOrDefault(fd => fd.Remarks.Contains(code));
                if (dbParty != null)
                {
                    partyId = dbParty.ID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return partyId;
        }

        public static string ConcateCustomerIds(long agentId)
        {
            string customerIds = "";
            try
            {
                List<long> customers = GetCustomers(agentId);
                for (int i = 0; i < customers.Count; i++)
                {
                    if (customers.Count - i == 1)
                    {
                        customerIds += $"{customers[i]}";
                    }
                    else
                    {
                        customerIds += $"{customers[i]}|";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return customerIds;
        }
    }
}