﻿using Core.Ghorahi.Data.Interface;
using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Service.Models;
using Core.Ghorahi.Service.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Service.Implementation.Agents
{
    public class AgentService : IAgentService
    {
        private IRepository repository;
        public AgentService(IRepository repository)
        {
            this.repository = repository;
        }

        public ResponseMessage<List<SelectItem>> GetCustomersByAgent(long agentId)
        {
            ResponseMessage<List<SelectItem>> responseMessage = new ResponseMessage<List<SelectItem>>
            {
                ResponseValue = new List<SelectItem>()
            };
            try
            {
                string agentIdStr = $"ID|{agentId}";
                List<string> parties = repository.InventoryContext.C_AgentPartyMap.Where(w => w.Agent.Equals(agentIdStr, StringComparison.InvariantCultureIgnoreCase)).Select(s => s.Party).ToList();
                responseMessage.ResponseValue = repository.InventoryContext.Parties.Where(w => w.IsActive && w.PartyTypeID.Equals(-101) && parties.Contains(w.Remarks)).Select(s => new SelectItem { Id = s.ID, Name = s.Name }).OrderBy(o => o.Name).ToList();
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<List<SelectItem>> GetAgents()
        {
            ResponseMessage<List<SelectItem>> responseMessage = new ResponseMessage<List<SelectItem>>
            {
                ResponseValue = new List<SelectItem>()
            };
            try
            {
                responseMessage.ResponseValue = repository.Context.ShipmentAgents.Where(w => (bool)w.IsActive).Select(s => new SelectItem { Id = s.ID, Name = s.CompanyName }).OrderBy(o => o.Name).ToList();
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }
    }
}