﻿using Core.Ghorahi.Service.Models.Messages;
using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Service.Models;
using Core.Ghorahi.Data.Interface;
using System.Collections.Generic;
using System.Linq;
using System;
using Core.Ghorahi.Service.Models.Stocks;
using Core.Ghorahi.Service.Models.Stocks.Request;
using Core.Ghorahi.Data.Entity.Inventory;

namespace Core.Ghorahi.Service.Implementation.Stocks
{
    public class StockService : IStockService
    {
        private IRepository repository;
        public StockService(IRepository repository)
        {
            this.repository = repository;
        }

        public ResponseMessage<ItemPriceRate> GetStockItemPriceRate(ItemPriceRateRequest itemPriceRateRequest)
        {
            ResponseMessage<ItemPriceRate> responseMessage = new ResponseMessage<ItemPriceRate>
            {
                ResponseValue = new ItemPriceRate()
            };
            try
            {
                StockItemSpecialRate dbItemSpecialRate = repository.InventoryContext.StockItemSpecialRates.Where(w => w.StockItem.Remarks.Equals(itemPriceRateRequest.StockItemId.ToString()) && w.PartyID.Equals(itemPriceRateRequest.PartyId)).OrderByDescending(o => o.WEFDatestamp).FirstOrDefault();
                if (dbItemSpecialRate != null && dbItemSpecialRate.Rate > 0)
                {
                    responseMessage.ResponseValue.Rate = decimal.Parse(dbItemSpecialRate.Rate.ToString("F"));
                    responseMessage.ResponseValue.Discount = decimal.Parse(dbItemSpecialRate.Discount.ToString("F"));
                }
                else
                {
                    StockItemRate dbStockItemRate = repository.InventoryContext.StockItemRates.Where(w => w.StockItem.Remarks.Equals(itemPriceRateRequest.StockItemId.ToString())).OrderByDescending(o => o.WEFDatestamp).FirstOrDefault();
                    if (dbStockItemRate != null)
                    {
                        responseMessage.ResponseValue.Rate = decimal.Parse(dbStockItemRate.Rate.ToString("F"));
                        responseMessage.ResponseValue.Discount = 0;
                    }
                }
                StockItemSundryMaster dbItemSundryMater = repository.InventoryContext.StockItemSundryMasters.Where(w => w.StockItem.Remarks.Equals(itemPriceRateRequest.StockItemId.ToString())).OrderByDescending(o => o.WEFDatestamp).FirstOrDefault();
                if (dbItemSundryMater != null)
                {
                    responseMessage.ResponseValue.Vat = dbItemSundryMater.VAT;
                    responseMessage.ResponseValue.ServiceTax = dbItemSundryMater.ServiceTax;
                    responseMessage.ResponseValue.ServiceTaxEC = dbItemSundryMater.ServiceTaxEC;
                    responseMessage.ResponseValue.ServiceTaxSHEC = dbItemSundryMater.ServiceTaxSHEC;
                }
                responseMessage.ResponseValue.StockItemId = itemPriceRateRequest.StockItemId;
                responseMessage.ResponseValue.PartyId = itemPriceRateRequest.PartyId;
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }

        public ResponseMessage<List<SelectItem>> GetStockItems()
        {
            ResponseMessage<List<SelectItem>> responseMessage = new ResponseMessage<List<SelectItem>>
            {
                ResponseValue = new List<SelectItem>()
            };
            try
            {
                long[] itemIds = { 15283, 15890 };
                responseMessage.ResponseValue = repository.Context.StockItems.Where(w => (bool)w.IsFinishedGood && itemIds.Contains(w.ID)).Select(s => new SelectItem { Id = s.ID, Name = s.Name }).ToList();
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }
    }
}