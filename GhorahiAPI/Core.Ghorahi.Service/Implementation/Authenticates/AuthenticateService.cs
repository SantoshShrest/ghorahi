﻿using Core.Ghorahi.Service.Models.Users.Result;
using Core.Ghorahi.Service.Models.Messages;
using Core.Ghorahi.Service.Models.Users;
using Core.Ghorahi.Common.Enumeration;
using Core.Ghorahi.Service.Interfaces;
using Core.Ghorahi.Data.Interface;
using System.Linq;
using System;
using Core.Ghorahi.Common.Manager;
using Core.Ghorahi.Data.Entity;

namespace Core.Ghorahi.Service.Implementation.Authenticates
{
    public class AuthenticateService : IAuthenticateService
    {
        private IRepository repository;
        public AuthenticateService(IRepository repository)
        {
            this.repository = repository;
        }
        public ResponseMessage<AuthenticateResult> VerifyUser(AuthenticateUser authenticateUser)
        {
            ResponseMessage<AuthenticateResult> responseMessage = new ResponseMessage<AuthenticateResult>
            {
                ResponseValue = new AuthenticateResult()
            };
            try
            {
                Data.Entity.Inventory.Party dbUser = repository.InventoryContext.Parties.FirstOrDefault(a => a.Email.Equals(authenticateUser.UserName, StringComparison.InvariantCultureIgnoreCase) && a.AddressLine2.Equals(authenticateUser.Password));
                if (dbUser != null)
                {
                    if (dbUser.KeyExpireDateTime != null && dbUser.KeyExpireDateTime > System.DateTime.Now)
                    {
                        responseMessage.ResponseValue.ApiKey = dbUser.ApiKey;
                    }
                    else
                    {
                        dbUser.KeyExpireDateTime = System.DateTime.Now.AddDays(30);
                        dbUser.ApiKey = KeyManager.GenerateNewApiKey(dbUser.Email);
                        int result = repository.InventoryContext.SaveChanges();
                        responseMessage.ResponseValue.ApiKey = dbUser.ApiKey;
                    }
                    responseMessage.ResponseValue.IsVerified = true;
                    responseMessage.ResponseValue.Name = dbUser.Name;
                    responseMessage.ResponseValue.UserType = dbUser.PartyTypeID;
                    if (dbUser.PartyTypeID.Equals(-101))
                    {
                        responseMessage.ResponseValue.CustomerId = dbUser.ID;
                        int partyId = int.Parse(dbUser.Remarks.Split('|')[1]);
                        SahyogiCustomerInfo dbSahyogiCustomer = repository.Context.SahyogiCustomerInfoes.Include("SahyogiCustomerPointsInfoes").FirstOrDefault(fd => fd.CustomerId.Equals(partyId));
                        if (dbSahyogiCustomer != null)
                        {
                            responseMessage.ResponseValue.HasSahabhagiScheme = dbSahyogiCustomer.SahyogiCustomerPointsInfoes.Count > 0 ? true : false;
                        }
                        Data.Entity.Inventory.C_AgentPartyMap dbAgentPartyMap = repository.InventoryContext.C_AgentPartyMap.FirstOrDefault(fd => fd.Party.Equals(dbUser.Remarks, StringComparison.InvariantCultureIgnoreCase));
                        if (dbAgentPartyMap != null)
                        {
                            responseMessage.ResponseValue.AgentId = long.Parse(dbAgentPartyMap.Agent.Split('|')[1]);
                        }
                    }
                    else if (dbUser.PartyTypeID.Equals(-102))
                    {
                        responseMessage.ResponseValue.AgentId = long.Parse(dbUser.Remarks.Split('|')[1]);
                    }
                }
                else
                {
                    Data.Entity.Inventory.AspNetUser dbAspUser = repository.InventoryContext.AspNetUsers.FirstOrDefault(fd => fd.Email.Equals(authenticateUser.UserName, StringComparison.InvariantCultureIgnoreCase));
                    if (dbAspUser != null && authenticateUser.Password.Equals("sagarmatha9", StringComparison.InvariantCulture))
                    {
                        responseMessage.ResponseValue.AdminId = dbAspUser.Id;
                        responseMessage.ResponseValue.UserType = 786;
                        responseMessage.ResponseValue.ApiKey = $"{dbAspUser.PasswordHash}{dbAspUser.SecurityStamp}".Replace("-", "").ToLowerInvariant();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return responseMessage;
        }
    }
}
