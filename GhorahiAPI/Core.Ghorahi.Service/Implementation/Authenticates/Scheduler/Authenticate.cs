﻿using Core.Ghorahi.Data;
using Core.Ghorahi.Data.Entity.Inventory;
using Core.Ghorahi.Data.Interface;
using Core.Ghorahi.Service.Models.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Ghorahi.Service.Implementation.Authenticates.Scheduler
{
    public sealed class Authenticate
    {
        private static IRepository Repository { get { return new Repository(); } }

        public static long GetInventoryAgentId(long agentId, long type)
        {
            long id = 0;
            try
            {
                string agentCode = $"ID|{agentId}";
                Party dbParty = Repository.InventoryContext.Parties.FirstOrDefault(fd => fd.Remarks.Equals(agentCode, StringComparison.InvariantCultureIgnoreCase));
                if (dbParty != null)
                {
                    id = dbParty.ID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return id;
        }

        public static ResponseMessage<bool> VerifyToken(long userId, string token)
        {
            ResponseMessage<bool> responseMessage = new ResponseMessage<bool>();
            try
            {
                Party dbParty = Repository.InventoryContext.Parties.FirstOrDefault(fd => fd.ID.Equals(userId) && fd.ApiKey.Equals(token) && fd.KeyExpireDateTime > System.DateTime.Now);
                if (dbParty != null)
                {
                    responseMessage.ResponseValue = true;
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }

        public static ResponseMessage<bool> VerifyToken(string adminId, string token)
        {
            ResponseMessage<bool> responseMessage = new ResponseMessage<bool>();
            try
            {
                AspNetUser dbAspUser = Repository.InventoryContext.AspNetUsers.FirstOrDefault(fd => fd.Id.Equals(adminId));
                if (dbAspUser != null)
                {
                    string hashToken = $"{dbAspUser.PasswordHash}{dbAspUser.SecurityStamp}".Replace("-", "").ToLowerInvariant();
                    responseMessage.ResponseValue = hashToken.Equals(token, StringComparison.InvariantCulture);
                }
            }
            catch (Exception ex)
            {
                responseMessage.HasError = true;
                throw ex;
            }
            return responseMessage;
        }
    }
}